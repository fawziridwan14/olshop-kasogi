-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 05 Okt 2017 pada 05.22
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_shop`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `addressbook`
--

CREATE TABLE `addressbook` (
  `id_address` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `name` varchar(56) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `address` text NOT NULL,
  `province` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `addressbook`
--

INSERT INTO `addressbook` (`id_address`, `customer_id`, `name`, `phone_number`, `address`, `province`, `city`) VALUES
(1, 14, 'rinalda', '9888787', 'Jalan kebun raya no 23', 'Jakarta', 'Jakarta Barat'),
(2, 14, 'hani', '9888787', 'Jalan kebun raya no 23', 'Jakarta', 'Jakarta Barat'),
(3, 14, 'ronie', '9888787', 'Jalan kebun raya no 23', 'Jakarta', 'Jakarta Barat'),
(4, 14, 'jonie', '9888787', 'Jalan kebun raya no 23', 'Jakarta', 'Jakarta Barat'),
(6, 14, 'Anton i oli', '2147483647', 'Jalan rawa buaya no 22 rt/rw 10/10 post 342', 'Sumatera Selatan', 'Palembang'),
(9, 14, 'binda', '9888787', 'Jalan kebun raya no 23', 'Jakarta', 'Jakarta Barat'),
(10, 14, 'yonita', '9888787', 'Jalan kebun raya no 23', 'Jakarta', 'Jakarta Barat'),
(11, 14, 'Bing', '998989', 'Jalan kebun jahe no 89', 'Jakarta', 'Jakarta Timur'),
(12, 15, 'blade', '89878798', 'Jalan rawa mangun jakarta timur no 90 rt/tw 90/30', 'Jakarta', 'Jakarta Timur'),
(13, 15, 'Rendy Siregar', '2147483647', 'Jalan rawa jaya no 90 tr/rw 90/90 patokan: depan komplek indah jaya', 'Medan', 'Medan Utara'),
(14, 14, 'vv', '434', 'vv', 'vv', 'vv'),
(15, 16, 'baba 12 12', '111111111111111', 'jalan hahah ahahah xxx', 'Jakarta x x', 'Jakarta Timurx'),
(20, 14, 'boyce', '82374832748', 'jalan jalan', 'Jakarta', 'Jakarta Barat'),
(21, 14, 'asdasd', '234234', 'sfdsf', '324', '234'),
(22, 14, 'Bool', '87979', 'jalan jalan haha 234 ', 'Jakarta', 'Jakarta Barat'),
(23, 14, 'bbbbbbbbbb', '23423423', '4sdfdsf', '23434', '234234324'),
(24, 14, 'Batittusa', '089088888888888', 'Jjjjjjjjjjjjjjjjjjjjjj', 'PROROROOR', 'JAJAJAJA'),
(25, 14, 'oooooooo', '888888888', 'kkkkkkkkkkk', 'jjjjjjjjjjjj', 'yyyyyyy'),
(26, 17, 'Billy Joe', '089898989', 'Jalan raya kembangan meruya utara no 90', 'Jakarta', 'Jakarta Barat'),
(27, 17, 'Indra Mukty', '0867989898', 'Jalan Let. Jen suparman kav 20 lt 50', 'Jakarta', 'Jakarta Barat'),
(28, 17, 'Leffy J.', '0817988998', 'Jalan condet raya no 90 Rt/rw 10/20', 'Jakarta', 'Jakarta Selatan'),
(29, 14, 'Kuuga Sharive', '088098989', 'Jalan Kebun Bunga No 1581 Rt/Rw 01/02 kode post : 13201', 'Sumatera Selatan', 'Palembang'),
(30, 18, 'Indah Permata Sari', '0878987879', 'Jalan Rawa Belong Raya no 90', 'DKI Jakarta', 'Jakarta Selatan'),
(31, 20, 'Shinji Kagawa', '081958315388', 'Jalan kebun jeruk raya no 11', 'DKI JAKARTA', 'JAKARTA BARAT'),
(32, 20, 'Kagawa Shinji', '0878989889', 'Jalan Kebun Sirih no 76', 'Sumatera Selatan', 'Palembang'),
(33, 21, 'fahsda', '082217666954', 'adhaksjdhakj', 'jawa barat', 'bandung'),
(34, 3, 'Fawzi Ridwan S', '0822165540540', 'Jl. kebon gedang 3', 'Jawa Barat', 'Bandung'),
(35, 22, 'asdjajk', '082216550540', 'hdajkh', 'dhskajhj', 'jdhak'),
(36, 2, 'fada', '082216550540', 'jhdkjhkj\r\n', 'jk', 'hkj'),
(37, 2, 'Ridwan', '082216550540', 'Jl. babakan sari 2', 'Jawa Barat', 'Bandung');

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rule` varchar(25) NOT NULL,
  `last_login_time` datetime NOT NULL,
  `image` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `email`, `username`, `password`, `rule`, `last_login_time`, `image`) VALUES
(1, 'kingcollection@gmail.com', 'kingcollection', 'bc21690b6700ef7442366da66a99cdfb', 'admin', '2017-09-23 17:59:52', 'avatar5.png'),
(3, 'fawziridwan@kingcollection.com', 'fawziridwan', '7c387ef9ad0df2aeb3aa86eb9368eb17', 'user', '2017-07-22 21:53:15', 'img.jpg'),
(4, 'fawzi@gmail.com', 'ridwanfawzi', 'fdf245855d3465b64bf1f612e9067ed4', 'root', '0000-00-00 00:00:00', 'user.png'),
(5, 'operator@gmail.com', 'operator', '4b583376b2767b923c3e1da60d10de59', 'root', '2017-08-03 07:45:04', '3x4.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `nama_bank` varchar(100) NOT NULL,
  `pemilik` varchar(100) NOT NULL,
  `no_rek` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bank`
--

INSERT INTO `bank` (`id`, `nama_bank`, `pemilik`, `no_rek`, `logo`) VALUES
(3, 'BRI', 'Hoffman Kasogi', '77090-8898-989', 'bri.png'),
(4, 'BCA', 'Arif', '0989-455-344', 'bca.png'),
(5, 'Mandiri', 'Fawzi Ridwan', '9999-9877-009', 'mandiri.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `berita_id` int(11) NOT NULL,
  `berita_kategori` int(11) DEFAULT NULL,
  `judul` varchar(100) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `isi` longtext NOT NULL,
  `tanggal` datetime NOT NULL,
  `jenis` enum('berita','halaman') NOT NULL,
  `status` enum('draf','publish') NOT NULL,
  `admin_id` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`berita_id`, `berita_kategori`, `judul`, `slug`, `isi`, `tanggal`, `jenis`, `status`, `admin_id`) VALUES
(1, 1, 'Tentang Kami', 'tentang-kami', '<h3>\r\n	Website Online E-Commerce</h3>\r\n<h3>\r\n	King Collection v1</h3>\r\n', '2017-08-06 09:17:39', 'halaman', 'draf', 'kingcollection'),
(2, 2, 'Cara Pemesanan', 'cara-pemesanan', '<p>\r\n	Cara Pemesanan</p>\r\n', '2017-08-06 09:25:14', 'halaman', 'publish', 'kingcollection');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita_kategori`
--

CREATE TABLE `berita_kategori` (
  `berita_kategori_id` int(11) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL,
  `slug` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita_kategori`
--

INSERT INTO `berita_kategori` (`berita_kategori_id`, `nama_kategori`, `slug`) VALUES
(1, 'Tentang Kami', 'tentang-kami'),
(2, 'Cara Pemesanan', 'cara-pemesanan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `brand`
--

INSERT INTO `brand` (`id`, `brand_name`) VALUES
(2, 'Nike'),
(3, 'Adidas'),
(4, 'Eiger');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `cart_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cart`
--

INSERT INTO `cart` (`id`, `product_id`, `qty`, `cart_code`) VALUES
(7, 8, 5, 'EUIHG74Q-4OABPQA3'),
(11, 8, 5, 'VM45NSG5-I3VYCYTD'),
(12, 237, 2, 'TQA58FRO-3E52QI5P'),
(13, 237, 2, 'V3T9IUX7-QRYI7GQC'),
(14, 237, 1, '2QI5CO8F-HEUXHTQA'),
(15, 237, 1, '6WI9CWZU-5ZB4F852'),
(22, 237, 1, 'GF8J29ES-VOT9E0NW'),
(23, 237, 1, 'OIJ69P7C-QPMAY65I'),
(29, 237, 1, '7ZHXMGO8-D83NY8WC'),
(30, 237, 1, 'VQ6FGSKQ-20K7RJ63'),
(31, 237, 1, 'AOISIOKW-UPW4MIUK'),
(32, 233, 1, 'ZSGUE7X9-7I5PH404'),
(33, 237, 1, 'TD4JAJ4Q-A9PF83V3'),
(35, 235, 1, 'MXBEF6D6-M49CBGQR'),
(36, 237, 1, 'MXBEF6D6-M49CBGQR'),
(37, 237, 1, 'ES6567IY-YTJ802SV'),
(39, 237, 2, 'CJ2ORW60-TFNJPFPM'),
(54, 237, 1, '6JGWXJCW-2FV9EJES'),
(55, 239, 1, 'CUZ56D8M-X3VJ2MR0'),
(57, 240, 1, 'HXFK7XYT-XWAJN960'),
(64, 240, 1, '0A5RDN74-80TMGS2Y'),
(65, 240, 2, '069VM4WZ-H2URJNFK'),
(66, 240, 1, 'C9ZS89XJ-GMK9ZO4J'),
(67, 240, 1, 'T047AOTF-AMKFGBXO'),
(68, 240, 1, 'GFE3GBZ9-967CD6WX'),
(69, 240, 1, '29PFXOAF-5N0VOAHK'),
(70, 240, 1, 'Q456J6QA-YCU2QVJ8'),
(71, 240, 1, '7CHASKQA-IOGFKFK7'),
(72, 240, 2, '3EYKYAWZ-5RQKDXOX'),
(73, 239, 1, 'TQ4SV36S-SRDPWZUV');

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`id`, `category_name`) VALUES
(2, 'Packs'),
(3, 'Tas Pria'),
(4, 'Tas Anak-anak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `confirm_payment`
--

CREATE TABLE `confirm_payment` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_code` varchar(17) NOT NULL,
  `text_detail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `confirm_payment`
--

INSERT INTO `confirm_payment` (`id`, `order_id`, `order_code`, `text_detail`) VALUES
(42, 0, '14205452', 'BRI#SYARIF#BCA 0989-455-344 a.n Kuuga#80000'),
(43, 0, '14544504', 'BRI#KUUGA#BCA 0989-455-344 a.n Kuuga#50000'),
(44, 0, '14540530', 'BCA#Nana Arnubu#BCA 0989-455-344 a.n Kuuga#324324'),
(45, 0, '14522450', 'BII#KUUGA RIDER#BCA 0989-455-344 a.n Kuuga#500000'),
(46, 0, '14403533', 'BCA#Shun gokong#BCA 0989-455-344 a.n Kuuga#500000'),
(47, 0, '14355425', 'BCA#kuuga#BCA 0989-455-344 a.n Kuuga#70000'),
(48, 0, '14055204', 'BCA#kuuga#BCA 0989-455-344 a.n Kuuga#5000000'),
(49, 0, '14420343', 'BCA#kuuga#BCA 0989-455-344 a.n Kuuga#400000'),
(50, 0, '14344043', 'sad#ds#BCA 0989-455-344 a.n Kuuga#234'),
(51, 0, '14543405', 'BCA#Shinta#BCA 0989-455-344 a.n Kuuga#1900000'),
(52, 0, '14330300', 'BANK MANDIRI#Shinta#BCA 0989-455-344 a.n Kuuga#1289000'),
(53, 0, '20300430', 'BCA#Shinji Kagawa#BCA 0989-455-344 a.n Kuuga#2774000'),
(54, 0, '3442543', 'BRI#Fawzi Ridwan#BRI 77090-8898-989 a.n Kuuga#700000'),
(55, 0, '2522024', 'BRI#Fawzi Ridwan#BCA 0989-455-344 a.n Kuuga#560000'),
(56, 0, '2243235', 'sadasda#daasd#BCA 0989-455-344 a.n Kuuga#900000'),
(57, 0, '2340304', 'BRI#Fawzi Ridwan#BCA 0989-455-344 a.n Kuuga#5000'),
(58, 0, '2052053', 'BRI#Ridwan#Mandiri 9999-9877-009 a.n Fawzi Ridwan#560000'),
(59, 0, '2345300', 'BRI#Fawziridwan#Mandiri 9999-9877-009 a.n Fawzi Ridwan#1120000'),
(60, 0, '2033330', 'BRI#ASAHDKA#Mandiri 9999-9877-009 a.n Fawzi Ridwan#3827877828'),
(61, 0, '2035022', 'BRI#Fawziridwan#Mandiri 9999-9877-009 a.n Fawzi Ridwan#345000'),
(62, 0, '2253000', 'BRI#Fawzi#Mandiri 9999-9877-009 a.n Fawzi Ridwan#415000'),
(63, 0, '2434200', 'BRI#Fawzi#Mandiri 9999-9877-009 a.n Fawzi Ridwan#396000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_name`, `email`, `password`) VALUES
(1, 'fawzi ridwan supiyaddin', 'fawziridwan@kingcollection.co.id', '7c387ef9ad0df2aeb3aa86eb9368eb17'),
(2, 'Fawzi Ridwan', 'fawziridwan@gmail.com', '7c387ef9ad0df2aeb3aa86eb9368eb17'),
(5, 'rani', 'rani@gmail.com', '72eb875289f32115a2f50f6b056f8760'),
(6, 'maharani', 'rani05@gmail.com', '72eb875289f32115a2f50f6b056f8760'),
(7, 'ridwan', 'ridwan@gmail.com', 'df305e02810f30ea45639a7b4c36e8a5');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konfigurasi`
--

CREATE TABLE `konfigurasi` (
  `konfigurasi_id` int(11) NOT NULL,
  `nama_key` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `tipe` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konfigurasi`
--

INSERT INTO `konfigurasi` (`konfigurasi_id`, `nama_key`, `isi`, `tipe`) VALUES
(1, 'nama-aplikasi', 'neracashop', 'umum'),
(2, 'company-name', 'Neraca Shoes', 'umum'),
(3, 'company-address', 'Jalan Permindo No. 61 A', 'umum'),
(4, 'company-phone', '0751-21443', 'umum'),
(5, 'company-email', 'neraca@gmail.com', 'umum'),
(6, 'tema-aktif', 'lte', 'tema'),
(7, 'tema-logo', 'logo-c4ca4238a0b923820dcc509a6f75849b.png', 'tema');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mutasi`
--

CREATE TABLE `mutasi` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `toko_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mutasi`
--

INSERT INTO `mutasi` (`id`, `tanggal`, `toko_id`, `produk_id`, `qty`, `keterangan`) VALUES
(9, '2017-08-22', 1, 239, 1, 'ambil 1'),
(10, '2017-08-22', 1, 240, 1, 'ambil 1 tas '),
(11, '2017-08-22', 1, 239, 10, 'ambil tas 10'),
(12, '2017-08-22', 1, 239, 10, 'aaaaa'),
(13, '2017-08-22', 1, 240, 15, 'asdaskh'),
(14, '2017-08-22', 1, 239, 10, 'asda');

-- --------------------------------------------------------

--
-- Struktur dari tabel `orderdetail`
--

CREATE TABLE `orderdetail` (
  `id` int(11) NOT NULL,
  `order_code` varchar(13) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `orderdetail`
--

INSERT INTO `orderdetail` (`id`, `order_code`, `order_id`, `product_id`, `qty`, `subtotal`) VALUES
(219, '2434200', 175, 239, 1, 345000),
(218, '2455252', 174, 240, 1, 420000),
(217, '2355555', 173, 240, 1, 420000),
(216, '2320323', 172, 240, 1, 420000),
(215, '2033343', 171, 239, 1, 345000),
(214, '2040404', 170, 240, 1, 420000),
(213, '2333300', 169, 240, 2, 840000),
(212, '2544530', 168, 240, 2, 840000),
(211, '2043334', 167, 240, 1, 420000),
(210, '2253000', 166, 239, 1, 345000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_code` varchar(17) NOT NULL,
  `order_date` date NOT NULL,
  `id_address` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `bank_transfer` varchar(50) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `ongkir` int(11) NOT NULL,
  `grandtotal` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `order_date`, `id_address`, `customer_id`, `bank_transfer`, `payment_status`, `ongkir`, `grandtotal`) VALUES
(170, '2040404', '2017-08-20', 37, 2, 'Mandiri 9999-9877-009 a.n Fawzi Ridwan', 0, 172000, 592000),
(168, '2544530', '2017-08-19', 36, 2, 'Mandiri 9999-9877-009 a.n Fawzi Ridwan', 0, 76000, 916000),
(169, '2333300', '2017-08-20', 37, 2, 'Mandiri 9999-9877-009 a.n Fawzi Ridwan', 0, 70000, 910000),
(167, '2043334', '2017-08-18', 36, 2, 'Mandiri 9999-9877-009 a.n Fawzi Ridwan', 0, 172000, 592000),
(166, '2253000', '2017-08-18', 36, 2, 'Mandiri 9999-9877-009 a.n Fawzi Ridwan', 1, 70000, 415000),
(171, '2033343', '2017-08-25', 36, 2, 'Mandiri 9999-9877-009 a.n Fawzi Ridwan', 0, 0, 0),
(172, '2320323', '2017-08-25', 37, 2, 'Mandiri 9999-9877-009 a.n Fawzi Ridwan', 0, 0, 0),
(173, '2355555', '2017-08-25', 36, 2, 'Mandiri 9999-9877-009 a.n Fawzi Ridwan', 0, 105040, 525040),
(174, '2455252', '2017-09-09', 37, 2, 'Mandiri 9999-9877-009 a.n Fawzi Ridwan', 0, 68000, 488000),
(175, '2434200', '2017-09-09', 37, 2, 'BCA 0989-455-344 a.n Arif', 1, 51000, 396000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permintaan`
--

CREATE TABLE `permintaan` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `supplier_id` varchar(50) NOT NULL,
  `produk_id` varchar(50) NOT NULL,
  `jumlah` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `permintaan`
--

INSERT INTO `permintaan` (`id`, `tanggal`, `supplier_id`, `produk_id`, `jumlah`) VALUES
(2, '2017-08-20', '1', '239', 1),
(3, '2017-08-21', '1', '240', 1),
(4, '2017-08-21', '1', '240', 1),
(5, '2017-08-21', '1', '240', 1),
(6, '2017-08-21', '1', '240', 1),
(7, '2017-08-21', '1', '240', 1),
(8, '2017-08-21', '1', '240', 1),
(9, '2017-08-21', '1', '240', 1),
(10, '2017-08-21', '1', '240', 1),
(11, '2017-08-21', '1', '240', 1),
(12, '2017-08-21', '1', '240', 1),
(13, '2017-08-21', '1', '240', 1),
(14, '2017-08-21', '1', '240', 1),
(15, '2017-08-21', '1', '240', 1),
(16, '2017-08-21', '1', '240', 1),
(17, '2017-08-21', '1', '240', 1),
(18, '2017-08-21', '1', '240', 1),
(19, '2017-08-21', '1', '240', 1),
(20, '2017-08-21', '1', '240', 1),
(21, '2017-08-21', '1', '240', 1),
(22, '2017-08-21', '1', '240', 1),
(23, '2017-08-21', '1', '240', 1),
(24, '2017-08-21', '1', '240', 1),
(25, '2017-08-21', '1', '240', 1),
(26, '2017-08-21', '1', '240', 1),
(27, '2017-08-21', '1', '240', 1),
(28, '2017-08-21', '1', '240', 1),
(29, '2017-08-21', '1', '240', 1),
(30, '2017-08-21', '1', '240', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_name` varchar(555) NOT NULL,
  `category_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(555) DEFAULT NULL,
  `stock` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `jumlah_masuk` int(11) NOT NULL,
  `jumlah_keluar` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`id`, `product_name`, `category_id`, `price`, `description`, `image`, `stock`, `brand_id`, `berat`, `jumlah_masuk`, `jumlah_keluar`) VALUES
(240, 'Diario Raven 28L', 2, 420000, 'Diario Raven 28L', '2.jpg', -3, 4, 4000, 3, 16),
(239, 'Eiger LS Solidus 1989 25L Navy Blue', 2, 345000, 'Eiger LS Solidus 1989 25L Navy Blue', '1.jpg', 9, 4, 3000, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `nama_supplier` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(30) NOT NULL,
  `id_admin` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `supplier`
--

INSERT INTO `supplier` (`id`, `nama_supplier`, `alamat`, `telepon`, `id_admin`) VALUES
(1, 'Sentra Tas Binong', 'jl. 123', '082216550540', 'kingcollection'),
(2, 'Toko Sepatu Rahmat', 'Jl.abc no 3', '082216550548', 'kingcollection');

-- --------------------------------------------------------

--
-- Struktur dari tabel `toko`
--

CREATE TABLE `toko` (
  `toko_id` int(11) NOT NULL,
  `nama_toko` varchar(100) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `telepon` varchar(30) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `tipe` enum('pusat','cabang') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `toko`
--

INSERT INTO `toko` (`toko_id`, `nama_toko`, `alamat`, `telepon`, `kota`, `tipe`) VALUES
(1, 'Toko Sukses Makmur', 'Jl. BKR no 7', '08221550540', 'Bandung', 'cabang'),
(2, 'Toko Mugi Laris', 'Jl. Kiaracondong no 23', '08221550541', 'Bandung', 'pusat');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addressbook`
--
ALTER TABLE `addressbook`
  ADD PRIMARY KEY (`id_address`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`berita_id`),
  ADD KEY `berita_kategori` (`berita_kategori`);

--
-- Indexes for table `berita_kategori`
--
ALTER TABLE `berita_kategori`
  ADD PRIMARY KEY (`berita_kategori_id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `confirm_payment`
--
ALTER TABLE `confirm_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
  ADD PRIMARY KEY (`konfigurasi_id`),
  ADD UNIQUE KEY `nama_key` (`nama_key`);

--
-- Indexes for table `mutasi`
--
ALTER TABLE `mutasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permintaan`
--
ALTER TABLE `permintaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toko`
--
ALTER TABLE `toko`
  ADD PRIMARY KEY (`toko_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addressbook`
--
ALTER TABLE `addressbook`
  MODIFY `id_address` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `berita_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `berita_kategori`
--
ALTER TABLE `berita_kategori`
  MODIFY `berita_kategori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `confirm_payment`
--
ALTER TABLE `confirm_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
  MODIFY `konfigurasi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `mutasi`
--
ALTER TABLE `mutasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `orderdetail`
--
ALTER TABLE `orderdetail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=220;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;
--
-- AUTO_INCREMENT for table `permintaan`
--
ALTER TABLE `permintaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `toko`
--
ALTER TABLE `toko`
  MODIFY `toko_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

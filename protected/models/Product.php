<?php

/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property integer $id
 * @property string $product_name
 * @property integer $category_id
 * @property integer $price
 * @property string $description
 * @property string $image
 * @property integer $stock
 * @property integer $brand_id
 * @property integer $berat
 */
class Product extends CActiveRecord 
{
	public $varPrice;
	/*ini digunakan untuk memformat mata uang produk
	 *yang akan ditampilkan*/
	protected function afterFind(){
		parent::afterFind();
		$this->varPrice=number_format($this->price,0,',','.');
		return TRUE;       
	} 
	
	/*ini digunakan untuk mreplace tanda titik(.) yang ada di mata uang
	 *seperti (100.000) menjadi (100000)*/
	protected function beforeSave(){
		parent::afterFind();
		$this->price = str_replace('.','',$this->price);
		return TRUE;       
	}	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_name, category_id, jumlah_masuk, jumlah_keluar, price, description, stock, brand_id, berat', 'required'),
			array('category_id, price, stock, jumlah_masuk,jumlah_keluar, brand_id, berat', 'numerical', 'integerOnly'=>true),
			array('product_name, image', 'length', 'max'=>555),
			array('berat', 'length', 'max'=>30),
			array('image', 'file', 'types'=>'jpg, gif, png','allowEmpty'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, product_name, category_id, jumlah_masuk, jumlah_keluar, price, berat, description, image, stock, brand_id, berat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category'=>array(self::BELONGS_TO,'category','category_id'),
			'brand'=>array(self::BELONGS_TO,'brand','brand_id'),			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_name' => 'Product Name',
			'category_id' => 'Category',
			'price' => 'Price',
			'description' => 'Description',
			'image' => 'Image',
			'stock' => 'Stock',
			'brand_id' => 'Brand',
			'berat' => 'Berat',
			'jumlah_masuk' => 'Jumlah Masuk',
			'jumlah_keluar' => 'Jumlah Keluar',			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('product_name',$this->product_name,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('price',$this->price);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('stock',$this->stock);
		$criteria->compare('brand_id',$this->brand_id);
		$criteria->compare('berat',$this->berat);
		$criteria->compare('jumlah_masuk',$this->jumlah_masuk);
		$criteria->compare('jumlah_keluar',$this->jumlah_keluar);		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

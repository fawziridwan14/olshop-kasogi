<?php

/**
 * This is the model class for table "berita".
 *
 * The followings are the available columns in table 'berita':
 * @property integer $berita_id
 * @property integer $berita_kategori
 * @property string $judul
 * @property string $slug
 * @property string $isi
 * @property string $tanggal
 * @property string $jenis
 * @property string $status
 * @property integer $admin_id
 */
class Berita extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'berita';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('judul, slug, isi, tanggal, jenis, status', 'required'),
			array('berita_kategori', 'numerical', 'integerOnly'=>true),
			array('judul', 'length', 'max'=>100),
			array('slug', 'length', 'max'=>200),
			array('admin_id', 'length', 'max'=>20),
			array('jenis, status', 'length', 'max'=>7),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('berita_id, berita_kategori, judul, slug, isi, tanggal, jenis, status, admin_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'berita_id' => 'Berita',
			'berita_kategori' => 'Berita Kategori',
			'judul' => 'Judul',
			'slug' => 'Slug',
			'isi' => 'Isi',
			'tanggal' => 'Tanggal',
			'jenis' => 'Jenis',
			'status' => 'Status',
			'admin_id' => 'Admin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('berita_id',$this->berita_id);
		$criteria->compare('berita_kategori',$this->berita_kategori);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('isi',$this->isi,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('jenis',$this->jenis,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('admin_id',$this->admin_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Berita the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

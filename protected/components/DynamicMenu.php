<?php 

Yii::Import('zii.widgets.CPortlet');

/**
* 
*/
class DynamicMenu extends CPortlet
{

	public function init()
	{
		parent::init();
	}	

	protected function renderContent()
	{
		$menu = Menu::model()->findAll();
		$this->render('dynamicmenu', array('menu'=>$menu));
	}

}
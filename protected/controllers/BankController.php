<?php

class BankController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin_page';
	const URLUPLOAD='/../images/bank/';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		isAuth::Admin();
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		isAuth::Admin();

		$model=new Bank;
		if(isset($_POST['Bank']))
		{
			$cekFile=$model->logo=  CUploadedFile::getInstance($model, 'logo');
			if (empty($cekFile)) {
				$model->attributes=$_POST['Bank'];
                $model->save();
			} else {
				$model->attributes=$_POST['Bank'];
                $model->logo=  CUploadedFile::getInstance($model, 'logo');
	
				if($model->save()){
					$model->logo->saveAs(Yii::app()->basePath.self::URLUPLOAD.$model->logo.'');			
					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}
		
		/*$model=new Bank;

		if(isset($_POST['Bank']))	{
			$model->attributes=$_POST['Bank'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}*/

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		isAuth::Admin();
		$model=$this->loadModel($id);

 		if(isset($_POST['Bank']))	{
 			$cekFile=$model->logo=  CUploadedFile::getInstance($model, 'logo');
 			if (empty($cekFile)) {
 				$model->attributes=$_POST['Bank'];
                $model->save();
 			} else {
 				$model->attributes=$_POST['Bank'];
                $model->logo=  CUploadedFile::getInstance($model, 'logo');
	            if($model->save())	{
	            	$model->logo->saveAs(Yii::app()->basePath.self::URLUPLOAD.$model->logo.'');
	                $this->redirect(array('view','id'=>$model->id));
	            }
 			}
        }		

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		isAuth::Admin();
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		isAuth::Admin();
		$dataProvider=new CActiveDataProvider('Bank');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		isAuth::Admin();
		$model=new Bank('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Bank']))
			$model->attributes=$_GET['Bank'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Bank the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Bank::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Bank $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bank-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

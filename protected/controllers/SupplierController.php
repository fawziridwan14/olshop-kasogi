<?php

class SupplierController extends Controller
{
	public $layout = 'admin_page';
	const INDEX = 'index';
	
	public function actionView($id)
	{
		isAuth::Admin();
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function loadModel($id)
	{
		$model=Supplier::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionDelete($id){
		isAuth::Admin();
	    $model = new Supplier;
	    if($model->deleteByPk($id)){
	        $this->redirect(array(self::INDEX));
	    }else{
	        throw new CHttpException(404,'Data yang di minta tidak tersedia');
	    }
	} 

	public function actionUpdate($id){
		isAuth::Admin();		
	    $data=new Supplier;
	    $model=$data->findByPk($id);
	     
	    if(isset($_POST['Supplier'])){
	         $model->attributes=$_POST['Supplier'];
	         
	        if($model->save()){
	            $this->redirect(array('index'));
	        }
	    }
	      
	    $this->render('update',array('model'=>$model));
	}


	public function actionCreate()	{
		isAuth::Admin();
		$model=new Supplier;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Supplier']))
        {
            $model->attributes=$_POST['Supplier'];
            if($model->save())
                $this->redirect(array('index'/*,'id'=>$model->id*/));
        }

        $this->render('create',array(
            'model'=>$model,
        ));		
	}

	public function actionIndex()	{
		$model = new Supplier;
		$data = $model->findAll();
		$this->render('index',array('data'=>$data));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
<?php

class PermintaanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin_page';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','report'),
				'users'=>array('*'),
			),
/*			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionReport()
	{
		$model = new Permintaan();
		$rptPdf = array();
		$rptPdf = Permintaan::model()->findAll('id','nama_supplier','product_name');
		$this->render('exportpdf', array('rptPdf' => $rptPdf));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		isAuth::Admin();
		$model=new Permintaan;
		// $product = Product::model()->findAll();

		if(isset($_POST['Permintaan']))
		{
			$model->attributes=$_POST['Permintaan'];
			if($model->save())

				$update = Yii::app()->db->createCommand()
					->update('product',
						array('stock'=>new CDbExpression('stock + :test1', array(':test1'=>$model->jumlah)),
						),
						// array('stock'=>new CDbExpression('stock + :test2', array(':test2'=>$model->jumlah)),),
						'id=:produk_id',
						array(':produk_id'=>$model->produk_id)
					);
				$update = Yii::app()->db->createCommand()
					->update('product',
						array('jumlah_masuk'=>new CDbExpression('jumlah_masuk + :test2', array(':test2'=>$model->jumlah)),
						),
						// array('stock'=>new CDbExpression('stock + :test2', array(':test2'=>$model->jumlah)),),
						'id=:produk_id',
						array(':produk_id'=>$model->produk_id)
					);

				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
			// 'product'=>$product,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Permintaan']))
		{
			$model->attributes=$_POST['Permintaan'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$model = Permintaan::model()->findAll();
		$dataProvider=new CActiveDataProvider('Permintaan');*/

		$sql = "SELECT * FROM permintaan ORDER BY id";
		$cursor = Permintaan::model()->findAllBySql($sql);
		$this->render('index',array(
			'cursor'=>$cursor,
		));
	}

	/**
	 * Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Permintaan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Permintaan']))
			$model->attributes=$_GET['Permintaan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Permintaan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Permintaan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Permintaan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='permintaan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

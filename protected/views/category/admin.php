<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Category', 'url'=>array('index')),
	array('label'=>'Create Category', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle(); 
	return false;
});
$('.search-form form').submit(function(){
	$('#category-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<br>

<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Kelola Kategori</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('category/index'); ?>" class="btn btn-primary btn-flat btn-sm"><i class="fa fa-bars fa-lg"></i> List Kategori</a> 	
			<a href="<?php echo Yii::app()->createUrl('category/create'); ?>" class="btn btn-success btn-flat btn-sm"><i class="fa fa-plus fa-lg"></i> Create Kategori </a>
		</span>
	</div>
	<div style="margin: 10px;">
		<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
		<div class="search-form" style="display:none"> 
		<?php $this->renderPartial('_search',array('model'=>$model,)); ?>
		</div>		
	</div>	
	<div style="margin: 10px;">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'category-grid',
			'dataProvider'=>$model->search(),
			// 'filter'=>$model,
			'summaryText'=>'',
			'pager'=>array(
				'header'=>'',
				'firstPageLabel'=>'| <',
				'lastPageLabel'=>'> |',
				'nextPageLabel'=>'>',
				'prevPageLabel'=>'<',
			),
			'columns'=>array(
				'id',
				'category_name',
				array(
					'header'=>'Aksi',
					'class'=>'CButtonColumn',
					'template'=>'{update}{view}{delete}'
				),
			),
		)); ?>		
	</div>
</div>	

<style type="text/css">
	.last	{
		display: inline !important;
	}
</style>

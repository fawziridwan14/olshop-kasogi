<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form CActiveForm */
?>

<div class="horizontal-form" style="margin: 10px;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'category-form',
	'enableAjaxValidation'=>false,
)); ?> 

	<p class="alert alert-warning">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'category_name', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'category_name',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'category_name'); ?>
	</div>

	<div class="form-group pull-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-primary')); ?>
	</div>
	<br><br><br><br>
	
<?php $this->endWidget(); ?>

</div><!-- form -->
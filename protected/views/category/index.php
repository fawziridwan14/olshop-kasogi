<?php
/* @var $this CategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Categories',
);

$this->menu=array(
	array('label'=>'Create Category', 'url'=>array('create')),
	array('label'=>'Manage Category', 'url'=>array('admin')),
);
?>
<br><br>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Kategori</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('category/create'); ?>" class="btn btn-success btn-flat btn-sm"><i class="fa fa-plus"></i>  Buat Kategori</a>	 
			<a href="<?php echo Yii::app()->createUrl('category/admin'); ?>" class="btn btn-danger btn-flat btn-sm"><i class="fa fa-cog"></i>  Kelola Kategori </a>
		</span>
	</div>
	<div style="margin: 10px;">
		<?php $this->widget('zii.widgets.CListView', array(
			'dataProvider'=>$dataProvider,
			'itemView'=>'_view',
		)); ?>		
	</div>
	<br>
</div>	
<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Category', 'url'=>array('index')),
	array('label'=>'Manage Category', 'url'=>array('admin')),
);


?>
<br><br>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Tambah Bank Transfer</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('category/admin'); ?>" class="btn btn-danger"><i class="fa fa-cogs"></i> Kelola Kategori</a>
			<a href="<?php echo Yii::app()->createUrl('category/index'); ?>" class="btn btn-primary"><i class="fa fa-bars"></i> List Kategori</a>
		</span>
	</div>
	<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
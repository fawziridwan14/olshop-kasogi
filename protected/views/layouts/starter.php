<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/fontawesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/css/skins/skin-blue.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/css/skins/skin-black.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/css/skins/skin-green.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/datatables/dataTables.bootstrap.css">
<!-- select2 css -->
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/plugin/select2/css/select2.min.css">
<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/plugin/select2/js/select2.full.min.js"></script>

<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>

<!-- <script src="/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script> -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/js/app.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!DOCTYPE html>
<?php
  if (isset(Yii::app()->user->id)){
      $level = Admin::model()->findByPk(Yii::app()->user->id)->rule;
      $nama = Admin::model()->findByPk(Yii::app()->user->id)->username;
      $id = Admin::model()->findByPk(Yii::app()->user->id);
  }
?>

<?php
$nama = Yii::app()->user->username;
if(!Yii::app()->user->isGuest):
  $sql = Yii::app()->db->createCommand("select * from orders where payment_status = '0' ")->queryAll(); //It's return the Array
  $data = count($sql);
  if ($data == 0){
    $data ='' ;
  }else{
   $data = count($sql); 
  }
endif;
?>

<?php
$nama = Yii::app()->user->username;
if(!Yii::app()->user->isGuest):
  $sql = Yii::app()->db->createCommand("select * from permintaan where jumlah")->queryAll(); //It's return the Array
  $data2 = count($sql);
  if ($data2 == 0){
    $data2 ='' ;
  }else{
   $data2 = count($sql); 
  }
endif;
?>

<script>
function cek(){
    $.ajax({
        url: "js/cekpesan.php",
        cache: false,
        success: function(msg){
            $("#notifikasi").html(msg);
      $("#notifikasi2").html(msg);
        }
    });
      var waktu = setTimeout("cek()",3000);
}

$(document).ready(function(){
    cek();
  $(button).click();    
      
  
});

</script>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo CHtml::encode($this->pageTitle); ?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/fontawesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/css/skins/skin-blue.min.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/css/skins/skin-black.min.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/css/skins/skin-green.min.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/datatables/dataTables.bootstrap.css">
  <!-- select2 css -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/plugin/select2/css/select2.min.css">

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="<?php echo Yii::app()->request->baseUrl; ?>/admin" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>K</b>KC</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>King</b> Collection</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <?php if ($level=='admin'):?>
          <li class="dropdown messages-menu">
            <a href="<?php echo Yii::app()->createUrl('productMasuk/admin'); ?>" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-product-hunt" title="Lihat Pesan"></i>
              <span class="label label-primary" id="notifikasi"><?php echo $data2; ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header"><span id="notifikasi2"><b style="color: red;">(<?php echo $data2 ?>)</b></span> Permintaan Stok Product</li>
              <li>
                <ul class="menu">
                </ul>
              </li>
              <li class="footer"><a href="<?php echo Yii::app()->createUrl('permintaan/admin'); ?>">Lihat Semua</a></li>
            </ul>
          </li>
          <?php endif; ?>
          <?php if ($level=='admin'):?>
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-shopping-cart"></i>
              <span class="label label-warning" id="notifikasi"><?php echo $data ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header"><span id="notifikasi2"><b style="color: red;">(<?php echo $data ?>)</b></span> Permintaan Order</li>
              <li>
                <!-- Inner Menu: contains the notifications -->
                <ul class="menu">
                </ul>
              </li>
              <li class="footer"><a href="<?php echo Yii::app()->createUrl('orders/admin'); ?>">Lihat Semua</a></li>
            </ul>
          </li>
          <?php endif; ?>
          <!-- User Admin Menu -->
          <?php if(Yii::app()->user->isGuest): ?>
          <li class="dropdown user user-menu">
            <a href="<?php echo Yii::app()->createUrl('admin/index'); ?>" class="dropdown-toggle">
              <span class="hidden-xs"><i class="fa fa-sign-in pull-right"></i> Masuk</span>
            </a>
          </li>
          <?php endif; ?>
          <?php if(!Yii::app()->user->isGuest): ?>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
              <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.Yii::app()->user->adminImage.'','image', array("class"=>"user-image")); ?>
              <span class="hidden-xs"><?php echo Yii::app()->user->username; ?></span>
            </a>
            <ul class="dropdown-menu">
              <?php if(!Yii::app()->user->isGuest): ?>
              <li class="user-header">
                <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.Yii::app()->user->adminImage.'','image', array("class"=>"img-circle")); ?>
                <p>
                      <?php echo Yii::app()->user->username; ?> <!-- - Web Developer -->
                  <small>Member since <?php echo date("Y") ?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo Yii::app()->createUrl('manageadmin/update', array('id'=>$id->id)); ?>" class="btn btn-default btn-flat"><i class="fa fa-user"></i> Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo Yii::app()->request->baseUrl; ?>/site/logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                </div>
              </li>
              <?php endif; ?>
            </ul>
          </li>
          <?php endif; ?>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <?php if(isset(Yii::app()->user->rule) =='root'):?>
      <div class="user-panel">
        <div class="pull-left image">
          <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.Yii::app()->user->adminImage.'','image', array("class"=>"user-image img-circle")); ?>
          <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
        </div> 
        <div class="pull-left info">
            <p><?php echo Yii::app()->user->username; ?></p> 
            <a href="#"><i class="fa fa-circle text-success"></i> Online
          </a>
        </div>
      </div>   
      <?php endif; ?>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <!-- Optionally, you can add icons to the links -->
        <?php if(isset(Yii::app()->user->rule) =='admin'):?>
        <li class="header">Main Navigation</li>
        <!-- <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/ongkir/admin"><i class="fa fa-ship"></i> <span>Ongkir</span></a></li> -->
        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/manageadmin/admin"><i class="fa fa-users"></i> <span>Kelola User Admin</span></a></li>      
        <li class="treeview"> 
          <a href="#"><i class="fa fa-cube"></i> <span>Product</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/product/admin"><span>Product</span></a></li>
            <!-- <li><a href="<?php //echo Yii::app()->request->baseUrl; ?>/product/admin"><i class="fa fa-product-hunt"></i> <span>Stok</span></a></li> -->
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/brand/admin"><span>Brand</span></a></li>
            <li class="active"><a href="<?php echo Yii::app()->request->baseUrl; ?>/category/admin"><span>Category</span></a></li>
          </ul>
        </li>
        <li class="treeview"> 
          <a href="#"><i class="fa fa-shopping-cart"></i> <span> Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/orders/admin"><span> Order</span></a></li>
          <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/mutasi/admin"><span> Mutasi Stok</span></a></li>
          <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/permintaan/admin"><span> Permintaan Produk</span></a></li>
        </ul>  
        </li>  
        <li class="treeview"> 
        <a href="#"><i class="fa fa-user"></i> <span>Mitra</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/supplier/index">Supplier</a></li>
          <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/customer/index">Member</a></li>
          <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/toko/create">Toko</a></li>
        </ul>
        </li>
        <li class="treeview"> 
        <a href="#"><i class="fa fa-user"></i> <span>Konten Web</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/berita/index">Semua Halaman</a></li>
          <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/berita/create">Tambah Halaman</a></li>
          <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/beritakategori/admin">Kategori Halaman</a></li>
        </ul>
        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/bank/admin"><i class="fa fa-bank"></i> <span>Bank Transfer</span></a></li>        
        </li>
      </ul>
        <?php endif; ?>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
      <?php if(isset($this->breadcrumbs)):?>
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
          'links'=>$this->breadcrumbs,
        )); ?>
      <?php endif?>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <?php echo $content; ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      by Fawzi Ridwan 
    </div>
    <!-- Default to the left -->
    <strong>&copy; 2017 <a href="#">King Collection</a>.</strong>
  </footer>
</div>
<!-- ./wrapper -->

<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/plugin/select2/js/select2.full.min.js"></script>

<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>

<!-- <script src="/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script> -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/js/app.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
</body>
</html>

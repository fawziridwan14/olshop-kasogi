<div class="col-lg-9 col-md-9 col-sm-12">
	<div class="col-lg-12 col-sm-12 hidden-print">
		<span class="title">Kategori <?php echo $data["product_name"]; ?></span>
	</div> 
<?php 
	$i = 1;

	foreach($models as $data):
		if ($i%2==0) {
			$class="rightbox";
		} else {
			$class="leftbox";
		}
		
?>  
<div class="col-lg-4 col-sm-4 hero-feature text-center">
    <div class="thumbnail">
    	<a href="<?php echo Yii::app()->request->baseUrl; ?>/product/detail/<?php echo $data["id"]; ?>" class="link-p">
        	<img  src="<?php echo Yii::app()->request->baseUrl.'/images/products/thumbs/'. $data->image; ?>" alt="" class="def-img" style="position: absolute; width: 332px; height: auto; max-width: none; max-height: none; left: -45px; top: 0px;">
		</a>
		<div class="caption prod-caption">
			<h4><a href="<?php echo Yii::app()->request->baseUrl; ?>/product/detail/<?php echo $data["id"]; ?>"><?php echo $data->product_name; ?></a></h4>
			<p><a href="#" class="btn btn-default btn-md">Rp <?php echo $data["varPrice"]; ?></a></p>
			<p>
				<div class="btn-group">
					<?php if (!Yii::app()->user->isGuest) {
						// contoh
						echo CHtml::link('<i class="fa fa-pencil"></i> Update',array('update', 'id'=>$data->id),array(
						    'class'=>'btn btn-app btn-warning btn-sm',
						));	

					} ?>
					&nbsp;&nbsp;  
					<?php 
						echo CHtml::link('<i class="fa fa-bars"></i> Detail',array('detail', 'id'=>$data->id),array(
						    'class'=>'btn btn-app btn-success btn-sm',
						));
					?>
					&nbsp;&nbsp;
					<?php 
						echo CHtml::link('<i class="fa fa-shopping-cart"></i> Beli',array('addtocart', 'id'=>$data->id),array(
						    'class'=>'btn btn-app btn-primary btn-sm',
						));
					?>			            	
				</div>				
			</p>
		</div>			
    </div>

</div>    
<?php $i++; 
endforeach;?> 

<div style="float: right; margin-top: 5px; margin-bottom: 7px; margin-right: 18px;clear: both;">
	<?php $this->widget('CLinkPager', array(
	   'pages' => $pages,
	   //'cssFile'=>Yii::app()->request->baseUrl.'/css/pager2.css',
	   'header'=> '',
	   'firstPageLabel'=>' first',
	   'lastPageLabel'=>' last',
	   'nextPageLabel'=>'next',
	   'prevPageLabel'=>'previous',
	   'maxButtonCount'=>5,
	))?> 
</div> 
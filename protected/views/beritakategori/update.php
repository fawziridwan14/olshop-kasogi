<?php
/* @var $this BeritakategoriController */
/* @var $model KategoriBerita */

$this->breadcrumbs=array(
	'Kategori Beritas'=>array('index'),
	$model->berita_kategori_id=>array('view','id'=>$model->berita_kategori_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List KategoriBerita', 'url'=>array('index')),
	array('label'=>'Create KategoriBerita', 'url'=>array('create')),
	array('label'=>'View KategoriBerita', 'url'=>array('view', 'id'=>$model->berita_kategori_id)),
	array('label'=>'Manage KategoriBerita', 'url'=>array('admin')),
);
?>

<h1>Update KategoriBerita <?php echo $model->berita_kategori_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
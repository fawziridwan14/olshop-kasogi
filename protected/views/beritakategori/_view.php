<?php
/* @var $this BeritakategoriController */
/* @var $data KategoriBerita */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('berita_kategori_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->berita_kategori_id), array('view', 'id'=>$data->berita_kategori_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_kategori')); ?>:</b>
	<?php echo CHtml::encode($data->nama_kategori); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('slug')); ?>:</b>
	<?php echo CHtml::encode($data->slug); ?>
	<br />


</div>
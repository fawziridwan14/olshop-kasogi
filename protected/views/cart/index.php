<style>
	table {
		clear: both;
		border-collapse: collapse;
		margin-left: 5px;
		margin-top: 30px; 
	}
	td {
		padding: 4px;
		border: solid 1px #cce2f8;
	}
	th {
		padding: 4px;
		border: solid 1px #cce2f8;
	}
	#clean td {
		border: none;
	}
</style>
<!-- Featured -->
<div class="col-lg-9 col-md-9 col-sm-12">
	<div class="col-lg-12 col-sm-12 hidden-print">
		<span class="title">Keranjang Belanja</span>
	</div>
<?php
	if(empty($data)){
?>
<div class="alert alert-warning">Keranjang Belanja Anda Kosong<br><br>
	<a style="text-decoration: none;color:green;" href="<?php echo $this->createUrl("product/");?>"><label>Silahkan belanja</label></a>
</div>	
<?php }else{ ?>

	<div class="table">
		<table width="100%" class="table table-bordered">
		<thead>
			<th>Nama Produk</th>
			<th>Jumlah</th>
			<th>Harga</th>
			<th>Sub Total</th>
			<th>Aksi</th>
		</thead> 
			<?php // echo CHtml::beginForm(array('change')); 
			echo CHtml::beginForm('','get',array('class'=>'test') );
			?>
			<tbody>
				<?php 
				$sum2= 0;
				$i=1;foreach($data as $model): $sum=$sum+($model['price'] * $model['qty']);
				$sum2=$sum2 + ($model['berat']);
				?>
				<?php echo CHtml::hiddenField('id' . $i, $model['id'], array('maxlength' => 3, 'style' => "width:20px;text-align:center")); ?>		
				<tr>
				<td><?php echo $i; ?>-<?php echo $model['product_name']; ?><br/>
					Keterangan : 			</td>
				<td>
					<?php echo CHtml::numberField('qty' . $i, $model['qty'], array('maxlength' => 5, 'style' => "width:50px;text-align:center", 'class'=>'form-control')); ?><br/>
						</td>
						<td><b>Rp <b><?php echo number_format($model['price'], 0, '.', '.'); ?></b><br/>
							<!-- <del>Rp 169,000</del> (Promo Lebaran Rp 30,000)			 -->
						</td>
						<td><b>Rp <?php echo number_format($model['price'] * $model['qty'], 0, '.', '.'); ?></b></td>	
						<td align="center"><?php echo CHtml::link('<i class="btn btn-sm btn-danger  fa fa-trash"> Hapus</i>', array('remove', 'id' => $model['id'])); ?></td>	
				</tr>

			<?php $i++;endforeach;?>					
			<!-- <input type="hidden" name="info[1][rowid]" value="b2803ecbfc23d5fb85ed2da191089474"/> -->
			</tbody>
		<tfoot>
			<tr>
				<td colspan="3"><b>Total Bayar</b></td>
				<td><b >Rp <?=number_format($sum, 0, ",", ".") ?></b></td>
				<td style="display: none"><b class="txt-tb"> <?php echo $sum ?></b></td>
				<td></td>
			</tr>														
			<tr>
				<td colspan="3"><b>Berat</b></td>
				<td colspan="1"><b><?php echo $sum2." Gram"?>
				<td></td>
			</tr>
			<tr>
				<td colspan="3"><b>Grand Total</b></td>
				<td><b class=""></b>
				<input type="text" name="" class="txt-gt" value="">
				</td>
				<td></td>
			</tr>					
			<tr>
				<td colspan="1"><b>Pilih Provinsi</b></td>
				<td colspan="2">
					<select class="form-control " id="provinsi">
						
					</select>					
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="1"><b>Pilih Kota/Kabupaten</b></td>
				<td colspan="2">
					<select class="form-control" id="city">
						
					</select>	

				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="1"><b>Pilihan Kurir </b></td>
				<td colspan="2">
					<select class="form-control" id="kurir">
						<option id = "">Select Courir</option>	
						<option value = "jne">JNE</option>	
						<option value = "tiki">Tiki</option>	
						<option value = "pos">POS</option>	
					</select>	

				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="1"><b>Pilihan Paket </b></td>
				<td colspan="2">
					<select class="form-control" id="paket"></select>	
				</td>
				<td></td>
				<td></td>
			</tr>

			<tr>
				<td colspan="1"><b>Ongkos Kirim</b></td>
				<td colspan="2">
					<input type class="form-control" value="0" id="ongkir" name="Transaksi[ongkir]" >	
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="3">
				<?php echo CHtml::button('Belanja lagi', array('submit' => array('product/'),'class'=> 'btn btn-primary btn-flat')); ?>
				<?php echo CHtml::submitButton('Update Keranjang',array('class'=>'btn btn-warning btn-flat')); ?>
				</td>
				<td>&nbsp;</td>

				
				<td align="center">			

				<?php //echo CHtml::button('Selesai Belanja', array('submit' => array('cart/finishshop'),'class'=>'btn btn-success btn-flat')); ?>
				</td>
			</tr>
		</tfoot>
		</table>		
	<?php echo CHtml::endForm(); ?>

	<form action="/olshop-kasogi/cart/finishshop">
	<input type="hidden" class="form-control" value="" id="ongkir2" name="Transaksi[ongkir]" >	
	<input type="hidden" class="form-control" value="" id="gt2" name="Transaksi[grand_total]" >	
		<input  type="submit" value="Selesai Belanja" class="btn btn-success btn-flat">
	</form>        	
	<?php } ?>    

</div>
<!-- End Featured -->
<script type="text/javascript">
$.ajax({ 
    type: 'GET', 
    url: 'http://localhost/olshop-kasogi/cart/GetProv', 
    data: { get_param: 'value' }, 
    dataType: 'json',
    success: function (data) { 
        /*$.each(data.rajaongkir.results, function(index, element) {
            $('body').append($('<div>', {
                text: element.name
            })
            alert("tes");
            );
        });*/
        //alert(data.rajaongkir.results[1].province_id);
        listItems = "";
        for (var i = 0; i < data.rajaongkir.results.length; i++){
	      listItems+= "<option value='" + data.rajaongkir.results[i].province_id + "'>" + data.rajaongkir.results[i].province+ "</option>";
	    }
	    $("#provinsi").html(listItems);

	    //alert(listItems);
    }
});	

$("#provinsi").change(function () {
        var end = this.value;
        var firstDropVal = $('#pick').val();
        //alert(end);
        $.ajax({ 
	    type: 'GET', 
	    url: 'http://localhost/olshop-kasogi/cart/GetCity/'+end, 
	    data: { get_param: 'value' }, 
	    dataType: 'json',
	    success: function (data) { 
	        /*$.each(data.rajaongkir.results, function(index, element) {
	            $('body').append($('<div>', {
	                text: element.name
	            })
	            alert("tes");
	            );
	        });*/
	        //alert(data.rajaongkir.results[1].province_id);
	        listItems = "";
	        for (var i = 0; i < data.rajaongkir.results.length; i++){
		      listItems+= "<option value='" + data.rajaongkir.results[i].city_id + "'>" + data.rajaongkir.results[i].city_name+ "</option>";
		    }
		    $("#city").html(listItems);

		    //alert(listItems);
	    }
	});
});
xcity_id = 0;
$("#provinsi").change(function () {
        var end = this.value;
        xcity_id = end;
        var firstDropVal = $('#pick').val();
        //alert(end);
        $.ajax({ 
	    type: 'GET', 
	    url: 'http://localhost/olshop-kasogi/cart/GetCity/'+end, 
	    data: { get_param: 'value' }, 
	    dataType: 'json',
	    success: function (data) { 
	        /*$.each(data.rajaongkir.results, function(index, element) {
	            $('body').append($('<div>', {
	                text: element.name
	            })
	            alert("tes");
	            );
	        });*/
	        //alert(data.rajaongkir.results[1].province_id);
	        listItems = "";
	        for (var i = 0; i < data.rajaongkir.results.length; i++){
		      listItems+= "<option value='" + data.rajaongkir.results[i].city_id + "'>" + data.rajaongkir.results[i].city_name+ "</option>";
		    }
		    $("#city").html(listItems);

		    //alert(listItems);
	    }
	});
});


	$("#kurir").change(function () {
	        //alert(<?php echo $sum2;?>);
	        var endz = this.value;
	        var firstDropVal = $('#pick').val();
	        //alert(end);

	        $.ajax({ 
		    type: 'GET', 
		    url: 'http://localhost/olshop-kasogi/cart/GetHarga/dest_id/'+xcity_id+'/cour/'+endz+'/weight/'+<?php echo $sum2?>, 
		    data: { get_param: 'value' }, 
		    dataType: 'json',
		    success: function (data) { 
		        /*$.each(data.rajaongkir.results, function(index, element) {
		            $('body').append($('<div>', {
		                text: element.name
		            })
		            alert("tes");
		            );
		        });*/
		        //alert(data.rajaongkir.results[1].province_id);
		        //alert("tes");

		        //alert(data.rajaongkir.results[0].costs[2].service);
		      //  alert(xcity_id+endz+<?php echo $sum2?>);
		        listItems = "<option>Pilih Paket</option>";
		        for (var i = 0; i < data.rajaongkir.results[0].costs.length; i++){

			      listItems+= "<option value='" +data.rajaongkir.results[0].costs[i].cost[0].value+ "'>" +  data.rajaongkir.results[0].costs[i].service + "</option>";
			      
			      //alert("lil");
			    }
			    $("#paket").html(listItems);
			    
			    //alert(listItems);
		    }
		});
	});

	/*function numberWithCommas(x) {
	    var parts = x.toString().split(".");
	    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    return parts.join(".");
	}*/


	// $("#paket").change(function () {
		$(document).on("change","#paket",function(e){
			// alert("123");
	    //alert(<?php echo $sum2;?>);
	    var endz = this.value;
	    var firstDropVal = $('#pick').val();
	    //alert(end);
	    $('#ongkir').val(endz);
	    $('#ongkir2').val(endz);


	    var tb = $('.txt-tb').html();
	    var gt = parseInt(endz) +  parseInt(tb);
	    // alert(gt);
	    $(".txt-gt").val(gt);
	    $("#gt2").val(gt);
	});

</script>
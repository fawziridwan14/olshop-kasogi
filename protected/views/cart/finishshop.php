<!-- Featured -->
<div class="col-lg-9 col-md-9 col-sm-12">
	<div class="col-lg-12 col-sm-12 hidden-print">
		<span class="title">Pilih Alamat</span>
	</div>
	<form action="" method="post" style="margin-left: 10px; margin-right: 10px;">
		 
	<div style="clear: left;"></div>
	<?php
		$i=1;foreach($addressBooks as $address):
		($i%3===0) ? $div ='<div style="clear:left;"></div>':$div='';
		($i===1) ? $checked ='checked="checked"' : $checked='';
	?>
		<div style="float: left;margin:5px 0 0 5px;border:solid 1px #CCC;">
			<table width="200" height="66" class="table table-responsive">
				<tr>
					<td valign="top"><input type="radio" <?php echo $checked;?> name="ChooseAddress[id_address]" value="<?php echo $address->id_address;?>" /></td>
					<td valign="top">
						<strong><?php echo $address->name;?></strong><br>
						<?php echo $address->address;?>
						<?php echo $address->province;?>
						<?php echo $address->city;?><br>
						Telp./Hp : 
						<?php echo $address->phone_number;?>
					</td>
				</tr>
			</table>
		</div>
		<?php
		echo $div;
		$i++;
		endforeach;
		?>
	<div style="clear:left;"></div>
	<div class="flash-notice" style="margin:15px 0 0 5px;">
	<p style="margin-left: 5px;">Atau
		<strong><a href="<?php echo $this->createUrl('cart/finishshop',array('addNewAddress'=>'and use it'));?>">Buat alamat baru dan gunakan sebagai alamat pengiriman</a></strong>
	</p>
	</div>
	<?php
	foreach(Yii::app()->user->getFlashes() as $key=>$message){
		echo "<div style='margin-left:5px;' class='flash-".$key."'>".$message."'</div>";
	}
	?>
	<br>
	<p style="float: right;margin-right: 5px;">
		<input type="submit" value="Lanjutkan" onclick="return confirm('periksa kembali, apakah alamat kirim sudah benar?')" / class="btn btn-primary btn-flat">
	</p>
	</form>
	<div class="clearfix"></div>            	

</div>
<!-- End Featured -->
<!-- Featured -->
<div class="col-lg-9 col-md-9 col-sm-12">
	<div class="col-lg-12 col-sm-12 hidden-print">
		<span class="title">Gunakan 'Alamat' yang sudah ada dibawah ini:</span>
	</div>

	<div class="form-horizontal" style="padding-left: 20px;">
		<?php
			$this->renderPartial('/account/_formAddAddress',array(
			 	'model'=>$model,
			 ));
		?>
	</div>

</div>
<!-- End Featured -->
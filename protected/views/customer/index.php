<?php 

?>

<div class="box">

	<div class="box-header">
	  <h3 class="box-title">Data Customer</h3>
	</div>
	<div style="margin: 10px;">
		<a href="<?php echo Yii::app()->createUrl('customer/create'); ?>" class="btn btn-primary btn-flat btn-sm" title="List"><i class="fa fa-plus fa-lg"></i> Tambah</a>
	</div>
		<!-- /.box-header -->
		<div class="box-body">
		  <table id="example1" class="table table-bordered table-striped">	
			<thead>
	            <tr>
	              <th>Id</th>
	              <th>Nama Member</th>
	              <th>Email Member</th>
	              <th>Kata Sandi Member</th>
	              <th>Aksi</th>
	            </tr>
            </thead>
            <tbody>
				<?php foreach ($model as $data):	?>
            	<tr style="width: 100%">
            		<td><?php echo $data['customer_id']; ?></td>
            		<td><?php echo $data['customer_name']; ?></td>
            		<td><?php echo $data['email']; ?></td>
            		<td><?php echo $data['password']; ?></td>
            		<td align="center">
            			<a href="<?php echo Yii::app()->createUrl('customer/'.$data->customer_id); ?>" class="btn btn-info btn-flat btn-sm"><i class="fa fa-search fa-lg"></i></a>
            			<a href="<?php echo Yii::app()->createUrl('customer/update/'.$data->customer_id); ?>" class="btn btn-warning btn-flat btn-sm"><i class="fa fa-pencil fa-lg"></i></a>
            			<a href="<?php echo Yii::app()->createUrl('customer/delete/'.$data->customer_id); ?>" class="btn btn-danger btn-flat btn-sm" title="Hapus"><i class="fa fa-trash fa-sm"></i></a>
            		</td>
            	</tr>
				<?php endforeach; ?>
            </tbody>		  
		  </table>
		</div>
</div>
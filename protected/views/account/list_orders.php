<style>
	/*css ini untuk mengcustom tampilan datagridview*/
	.grid-view{
		margin-left:5px !important;
	}
	.summary{display:none !important;}
	.grid-view table.items [id*="order-grid"]{
		background: #be0f0f !important;
	}
	.grid-view table.items tr.odd{background: #f7d0d0 !important;}
	.grid-view table.items tr.even{background: #faeaea !important;}
	.grid-view table.items tr > td > a{
		text-decoration: none;
		color:black;	 
	}
	.grid-view table.items tr > td > a:hover{
		text-decoration: underline;
		color:#745151;	 
	}
</style>

<div class="col-lg-9 col-md-9 col-sm-12">
	<div class="col-lg-12 col-sm-12 hidden-print" style="text-align: justify;">
		<span class="title">Order History Anda</span>
	</div>

	<div>
		<?php $this -> renderPartial('_myaccount_menu'); ?>	
	</div>
	<?php
	foreach (Yii::app()->user->getFlashes() as $key => $message) {
		echo "<div class='alert alert-warning' style='margin:5px 5px 0 5px;' class='flash-" . $key . "'>" . $message . "</div>";
	}
	?> 

	<div style="clear: left;"></div>
	<?php
		/*buat code jquery untk search data order*/
		Yii::app() -> clientScript -> registerScript('search', "
			/*jika search form disumbit*/ 
			$('.search-form form').submit(function(){
				/*update order-grid*/	
				$.fn.yiiGridView.update('order-grid', {
				/*set data form menjadi serialize*/	
				data: $(this).serialize()
				});
			return false;
			});
		");
	?>
	<!--form search--> 
	<div class="search-form" style="margin-left: 15px; margin-right: 15px;">
		<form action="<?php Yii::app() -> createUrl($this ->route); ?>" method="get">
			<?php $form = $this -> beginWidget('CActiveForm'); ?>
			<div class="input-group">
			    <!-- <input type="text" name="cari" class="form-control input-search" placeholder="Cari berdasarkan Order Code"/> -->
				<?php echo $form -> textField($model, 'order_code',array('placeholder'=>'cari berdasarkan order code','size'=>30,'class'=>'form-control input-search')); ?>
			    <span class="input-group-btn">
			        <button class="btn btn-primary no-border-left" type="submit"><i class="fa fa-search"></i></button>
			    </span>
			</div>			
			<?php $this -> endWidget(); ?>
		</form>
		<?php //echo CHtml::submitButton('Search', array('class'=>'btn btn-default btn-flat')); ?>
	</div>

	<div style="clear: left;"></div>
	<div style="margin: 15px;">
	<?php $this -> widget('zii.widgets.grid.CGridView', array('id' => 'order-grid',
		'dataProvider' => $model -> search(),
		'summaryText'=>'',
		//'filter'=>$model,
		'columns' => array(
			'id', 
			array(
				'name'=>'order_code',
				'header'=>'Nomor Pemesanan',
				'type'=>'html',
				'value'=>'CHtml::link("$data->order_code", array("orders", "id"=>$data->id),array("title"=>"click to view order"))',
				 
			), 
			'order_date', 
			'bank_transfer',
			array(
				'name'=>'ongkir',
				'header'=>'Ongkos Kirim',
				'type'=>'html',
				'value'=>'number_format($data->ongkir,2,",",".")',
			),
			// 'ongkir'
			)
		)
		);
	?>		
	</div>


</div>	
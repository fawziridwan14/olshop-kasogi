<div class="row">
	<div class="container">
		<div class="col-lg-9 col-md-9 col-sm-12 hidden-print" style="text-align: justify;">
			<a href="<?php echo $this->createUrl('account/orders'); ?>" class="btn btn-primary"> Pesanan Saya</a> 
			<a href="<?php echo Yii::app()->request->baseUrl;?>/account/addressbook" class="btn btn-primary"><i class="fa fa-book"></i> Buku Alamat</a> 
			<a href="<?php echo $this->createUrl('account/info');?>" class="btn btn-primary"><i class="fa fa-user"></i> Informasi Akun</a> 
			<a href="<?php echo Yii::app()->request->baseUrl;?>/account/changepassword" class="btn btn-primary"><i class="fa fa-key"></i> Ubah Password</a>
		</div>		
	</div>	
</div>
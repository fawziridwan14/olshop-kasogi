<div class="col-lg-9 col-md-9 col-sm-12">
	<div class="col-lg-12 col-sm-12 hidden-print" style="text-align: justify;">
		<span class="title">Order History Anda</span>
	</div>

	<div>
		<?php $this -> renderPartial('_myaccount_menu'); ?>	
	</div>
	<br> 
	<div class="form-horizontal">
	<div style="clear: left;"></div>
 	<?php echo CHtml::beginForm();?>
 	<?php echo CHtml::errorSummary($model); ?>
	<div style="margin: 15px;">
			<table class="table table-responsive table-bordered">
		<tr>
			<td><?php echo CHtml::activeLabel($model, 'nomerPemesanan'); ?></td> 
			<td><b><?php echo $_GET['confirm'];?></b>
				 <?php echo CHtml::activeHiddenField($model,'nomerPemesanan',array( 'value'=>$_GET['confirm']));?> 
			</td>
		</tr>
		<tr>
			<td><?php echo CHtml::activeLabel($model, 'bankAsal',array('class'=>'label-control')); ?></td> 
			<td>
				 <?php echo CHtml::activeTextField($model,'bankAsal',array('class'=>'form-control'));?> 
			</td>
		</tr>
		<tr>
			<td><?php echo CHtml::activeLabel($model, 'pemilikRekAsal',array('class'=>'label-control')); ?></td> 
			<td>
				 <?php echo CHtml::activeTextField($model,'pemilikRekAsal', array('class'=>'form-control'));?> 
			</td>
		</tr>
		<tr>
			<td colspan="3"><hr style="color:pink;"></td>
		</tr>
		<tr>
			<td><?php echo CHtml::activeLabel($model, 'bankTujuan',array('class'=>'label-control')); ?></td> 
			<td>
				<select name="Confirmpayment[bankTujuan]" class="form-control">
				<?php $bk = Bank::model()->findAll(); 
					foreach($bk as $bank):
				?>
				<?php endforeach; ?>
					<option value="<?php echo $bank['nama_bank'] ?> <?php echo $bank['no_rek'] ?> a.n <?php echo $bank['pemilik'] ?>"><?php echo $bank['nama_bank'] ?> <?php echo $bank['no_rek'] ?> a.n <?php echo $bank['pemilik'] ?></option>
				</select>
			</td>
		</tr>
		<tr>
			<td><?php echo CHtml::activeLabel($model, 'nominalTransfer', array('class'=>'label-control')); ?></td> 
			<td><?php echo CHtml::activeTextField($model,'nominalTransfer', array('class'=>'form-control'));?></td>
		</tr>
		<tr>
			<td>&nbsp;</td> 
			<td class="pull-right"><?php echo CHtml::submitButton('Confirm', array('class'=>'btn btn-primary btn-flat btn-mds'));?></td>
		</tr>
		
		 
	</table>
	<?php echo CHtml::endForm();?>	 
	 

	<div style="clear: both;">
		&nbsp;
	</div>
	 
</div>

</div>
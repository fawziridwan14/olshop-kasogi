<div class="form-horizontal">

<?php $form = $this -> beginWidget('CActiveForm', 
array('id' => 'add-addressBook-form', 
    'enableAjaxValidation' => false,
    'enableClientValidation'=>TRUE, 
)); ?>

<p class="note">
            Fields with <span class="required">*</span> are required.
</p>

<?php echo $form -> errorSummary($model); ?>

<div class="form-group">
    <div class="col-md-8">
        <?php echo $form -> labelEx($model, 'name', array('class'=>'control-label')); ?>
        <?php echo $form -> textField($model, 'name', array('class'=>'form-control','placeholder'=>'Name','size' => 45, 'maxlength' => 45)); ?>
        <?php echo $form -> error($model, 'name'); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-md-8">
        <?php echo $form -> labelEx($model, 'phone_number', array('class'=>'control-label')); ?>
        <?php echo $form -> textField($model, 'phone_number', array('class'=>'form-control','placeholder'=>'No Telfon','size' => 45, 'maxlength' => 45)); ?>
        <?php echo $form -> error($model, 'phone_number'); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-md-8">
        <?php echo $form -> labelEx($model, 'address', array('class'=>'control-label')); ?>
        <?php echo $form -> textArea($model, 'address', array('class'=>'form-control','placeholder'=>'Alamat','size' => 45, 'maxlength' => 45)); ?>
        <?php echo $form -> error($model, 'address'); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-md-8">
        <?php echo $form -> labelEx($model, 'province', array('class'=>'control-label')); ?>
        <?php echo $form -> textField($model, 'province', array('class'=>'form-control','placeholder'=>'Provinsi/Daerah','size' => 45, 'maxlength' => 45)); ?>
        <?php echo $form -> error($model, 'province'); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-md-8">
        <?php echo $form -> labelEx($model, 'city', array('class'=>'control-label')); ?>
        <?php echo $form -> textField($model, 'city', array('class'=>'form-control','placeholder'=>'Kota','size' => 45, 'maxlength' => 45)); ?>
        <?php echo $form -> error($model, 'city'); ?>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-6 control-label">&nbsp;</label>
		<div class="col-md-5">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Tambah Alamat':'Save', array('class'=>'btn btn-primary btn-md')); ?>
		</div>
</div>
<?php $this -> endWidget(); ?>
</div>
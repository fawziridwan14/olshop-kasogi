<?php
/* @var $this BrandController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Brands',
);

$this->menu=array(
	array('label'=>'Create Brand', 'url'=>array('create')),
	array('label'=>'Manage Brand', 'url'=>array('admin')),
);
?>

<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Merek Produk</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('brand/create'); ?>" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah Merk Produk</a>
			<a href="<?php echo Yii::app()->createUrl('brand/admin'); ?>" class="btn btn-danger btn-flat"><i class="fa fa-gear"></i> Kelola Merk Produk</a>
		</span>
	</div>
	<?php $this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_view',
	)); ?>	
	<br>
</div>		

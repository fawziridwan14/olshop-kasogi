<?php
/* @var $this BrandController */
/* @var $model Brand */

$this->breadcrumbs=array(
	'Brands'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Brand', 'url'=>array('index')),
	array('label'=>'Manage Brand', 'url'=>array('admin')),
);
?>

<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Tambah Merek</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('brand/admin'); ?>" class="btn btn-danger"><i class="fa fa-cogs"></i> Kelola Merek</a>
		</span>
	</div>
	
	<?php $this->renderPartial('_form', array('model'=>$model)); ?> 

</div>	

<?php
/* @var $this BrandController */
/* @var $model Brand */

$this->breadcrumbs=array(
	'Brands'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Brand', 'url'=>array('index')),
	array('label'=>'Create Brand', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#brand-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Kelola Brand</h3>
				<span style="float: right;padding-top: 15px;">
				<a href="<?php echo Yii::app()->createUrl('brand/create'); ?>" class="btn btn-success btn-flat btn-sm"><i class="fa fa-plus"></i> Create Brand</a>
				<a href="<?php echo Yii::app()->createUrl('brand/index'); ?>" class="btn btn-primary btn-flat btn-sm"><i class="fa fa-list"></i> List Brand</a>
				</span>

            </div>



<div style="margin: 10px;">
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none"> 
<?php $this->renderPartial('_search',array('model'=>$model,)); ?>
</div>
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'brand-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'id',
			'brand_name',
			array(
				'header'=>'Aksi',
				'class'=>'CButtonColumn',
			),
		),
	)); ?>	
</div>

</div>
<?php

?>
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Data Toko Outlet/Cabang</h3>
	</div>
		<div style="margin: 10px;">
			<a href="<?php echo Yii::app()->createUrl('supplier/create'); ?>" class="btn btn-primary btn-flat btn-sm" title="List"><i class="fa fa-plus fa-lg"></i> Tambah</a>
			<?php 
				// echo CHtml::link('Tambah Supplier', array('create'));
				// echo '';
			?>			
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		  <table id="example1" class="table table-bordered table-striped">	
			<thead>
	            <tr>
					<th>Id</th>
					<th>Nama Supplier</th>
					<th>Alamat</th>
					<th>Nomor Telepon</th>
					<!-- <th>Id Admin</th> -->
					<th><center>Aksi</center></th>
	            </tr>
            </thead>
            <tbody>
            	<?php foreach($data as $model): ?>
            	<tr style="width: 100%">
            		<td><?php echo $model['id']; ?></td>
            		<td><?php echo $model['nama_supplier']; ?></td>
            		<td><?php echo $model['alamat']; ?></td>
            		<td><?php echo $model['telepon']; ?></td>
            		<!-- <td><?php //echo $model['id_admin']; ?></td> -->
            		<td align="center">
            			<a href="<?php echo Yii::app()->createUrl('supplier/view/'.$model->id); ?>" class="btn btn-default btn-flat btn-sm" title="List"><i class="fa fa-search fa-lg"></i></a>
            			<a href="<?php echo Yii::app()->createUrl('supplier/update/'.$model->id); ?>" class="btn btn-info btn-flat btn-sm" title="Edit"><i class="fa fa-pencil fa-lg"></i></a>
            			<a href="<?php echo Yii::app()->createUrl('supplier/delete/'.$model->id); ?>" class="btn btn-danger btn-flat btn-sm" title="Hapus"><i class="fa fa-trash fa-lg"></i></a>
            			<?php //echo CHtml::submitButton(CHtml::encode('Delete'), array('delete', 'id'=>$model->id));?>
            		</td>
            	</tr>
            	<?php endforeach; ?>
            </tbody>		  
		  </table>
		</div>
</div>
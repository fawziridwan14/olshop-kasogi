<?php

$this->menu=array(
	array('label'=>'List Supplier', 'url'=>array('index')),
	array('label'=>'Create Supplier', 'url'=>array('create')),
	array('label'=>'Update Supplier', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Supplier', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Suppliers', 'url'=>array('admin')),
);
?>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">View Suppliers #<?php echo $model->id; ?></h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('supplier/index'); ?>" class="btn btn-info" title="List Suppliers Transfer"><i class="fa fa-bars"></i></a>
			<a href="<?php echo Yii::app()->createUrl('supplier/create'); ?>" class="btn btn-primary" title="Tambah Suppliers Transfer"><i class="fa fa-plus"></i></a>
			<a href="<?php echo Yii::app()->createUrl('supplier/update/'.$model->id); ?>" class="btn btn-warning" title="Update Suppliers Transfer"><i class="fa fa-pencil"></i></a>
			<a href="<?php echo Yii::app()->createUrl('supplier/delete/'.$model->id); ?>" class="btn btn-danger" title="Delete Suppliers Transfer"><i class="fa fa-trash"></i></a>
			<a href="<?php echo Yii::app()->createUrl('Supplier/admin'); ?>" class="btn btn-success" title="Kelola Suppliers Transfer"><i class="fa fa-cogs"></i></a>
		</span>
	</div>
	<div style="margin: 10px;">
		<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				'id',
				'nama_supplier',
				'alamat',
				'telepon',
				// 'id_admin',
			),
		)); ?>		
	</div>	
	<br>
</div>	
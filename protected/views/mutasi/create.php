<?php
/* @var $this MutasiController */
/* @var $model Mutasi */

$this->breadcrumbs=array(
	'Mutasis'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Mutasi', 'url'=>array('index')),
	array('label'=>'Manage Mutasi', 'url'=>array('admin')),
);
?>

<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Tambah Data Mutasi Stok</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('mutasi/admin'); ?>" class="btn btn-danger"><i class="fa fa-cogs"></i> Kelola Data</a>
		</span>
	</div>
	<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
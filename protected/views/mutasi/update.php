<?php
/* @var $this MutasiController */
/* @var $model Mutasi */

$this->breadcrumbs=array(
	'Mutasis'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Mutasi', 'url'=>array('index')),
	array('label'=>'Create Mutasi', 'url'=>array('create')),
	array('label'=>'View Mutasi', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Mutasi', 'url'=>array('admin')),
);
?>

<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Update Mutasi <?php echo $model->id; ?></h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('mutasi/index'); ?>" class="btn btn-info btn-flat"><i class="fa fa-bars"></i> Lihat Mutasi Stok</a>
			<a href="<?php echo Yii::app()->createUrl('mutasi/create'); ?>" class="btn btn-success btn-flat"><i class="fa fa-plus"></i> Tambah Mutasi Stok</a>
			<a href="<?php echo Yii::app()->createUrl('mutasi/view', array('id'=>$model->id)); ?>" class="btn btn-primary btn-flat"><i class="fa fa-search"></i> Detail Mutasi Stok</a>
			<a href="<?php echo Yii::app()->createUrl('mutasi/admin'); ?>" class="btn btn-danger btn-flat"><i class="fa fa-cogs"></i> Kelola Data</a>
		</span>
	</div>
	<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
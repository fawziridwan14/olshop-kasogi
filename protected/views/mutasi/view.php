<?php
/* @var $this MutasiController */
/* @var $model Mutasi */

$this->breadcrumbs=array(
	'Mutasis'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Mutasi', 'url'=>array('index')),
	array('label'=>'Create Mutasi', 'url'=>array('create')),
	array('label'=>'Update Mutasi', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Mutasi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Mutasi', 'url'=>array('admin')),
);
?>

<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">View Mutasi #<?php echo $model->id; ?></h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('mutasi/index'); ?>" class="btn btn-primary btn-flat"><i class="fa fa-bars"></i> List Mutasi Stok</a>
			<a href="<?php echo Yii::app()->createUrl('mutasi/create'); ?>" class="btn btn-success btn-flat"><i class="fa fa-plus"></i> Tambah Mutasi Stok</a>
			<a href="<?php echo Yii::app()->createUrl('mutasi/update', array('id'=>$model->id)); ?>" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i> Update Mutasi Stok</a>
			<a href="<?php echo Yii::app()->createUrl('mutasi/admin'); ?>" class="btn btn-danger btn-flat"><i class="fa fa-cogs"></i> Kelola Data</a>
		</span>
	</div>
	<div style="margin: 10px; padding-bottom: 10px;">
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'id',
			'tanggal',
			array(
				'value' => $model->toko_relasi->nama_toko, 
				'label' => 'Nama Toko'
			),
			array(
				'value' => $model->product_relasi->product_name, 
				'label' => 'Nama Produk'
			),		
			'qty',
			'keterangan',
		),
	)); ?>		
	</div>
</div>
<?php
/* @var $this TokoController */
/* @var $model Toko */
/* @var $form CActiveForm */
?>

<div class="form-group" style="margin: 10px;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-group">
		<?php echo $form->label($model,'toko_id', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'toko_id', array('class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'nama_toko', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'nama_toko',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'alamat', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'alamat',array('size'=>60,'maxlength'=>200, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'telepon', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'telepon',array('size'=>30,'maxlength'=>30, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'kota', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'kota',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'tipe', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'tipe',array('size'=>6,'maxlength'=>6, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group pull-right">
		<?php echo CHtml::submitButton('Search', array('class'=>'btn btn-primary btn-flat')); ?>
	</div>
	<br><br>
<?php $this->endWidget(); ?>

</div><!-- search-form -->
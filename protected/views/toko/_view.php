<?php
/* @var $this TokoController */
/* @var $data Toko */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('toko_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->toko_id), array('view', 'id'=>$data->toko_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_toko')); ?>:</b>
	<?php echo CHtml::encode($data->nama_toko); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telepon')); ?>:</b>
	<?php echo CHtml::encode($data->telepon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kota')); ?>:</b>
	<?php echo CHtml::encode($data->kota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe')); ?>:</b>
	<?php echo CHtml::encode($data->tipe); ?>
	<br />


</div>
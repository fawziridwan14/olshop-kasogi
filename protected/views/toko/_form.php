<?php
/* @var $this TokoController */
/* @var $model Toko */
/* @var $form CActiveForm */
?>

<div class="form" style="margin: 10px;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'toko-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="alert alert-warning">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_toko', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'nama_toko',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'nama_toko'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'alamat', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'alamat',array('size'=>60,'maxlength'=>200,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'alamat'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'telepon', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'telepon',array('size'=>30,'maxlength'=>30,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'telepon'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kota', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'kota',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'kota'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tipe', array('class'=>'label-control')); ?>
		<?php echo ZHtml::enumDropDownList($model,'tipe',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'tipe'); ?>
	</div>

	<div class="form-group pull-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Save', array('class'=>'btn btn-primary btn-flat')); ?>
	</div>
	<br><br><br>
<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
/* @var $this TokoController */
/* @var $model Toko */

$this->breadcrumbs=array(
	'Tokos'=>array('index'),
	$model->toko_id=>array('view','id'=>$model->toko_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Toko', 'url'=>array('index')),
	array('label'=>'Create Toko', 'url'=>array('create')),
	array('label'=>'View Toko', 'url'=>array('view', 'id'=>$model->toko_id)),
	array('label'=>'Manage Toko', 'url'=>array('admin')),
);
?>

<br>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Ubah Toko #<?php echo $model->toko_id; ?></h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('toko/index'); ?>" class="btn btn-primary" title="List toko "><i class="fa fa-bars"></i></a>
			<!-- echo Yii::app()->createUrl('toko/delete/'.$model->id); -->
			<a href="<?php echo Yii::app()->createUrl('toko/'.$model->toko_id); ?>" class="btn btn-info" title="View toko "><i class="fa fa-search"></i></a>
			<a href="<?php echo Yii::app()->createUrl('toko/create'); ?>" class="btn btn-success" title="Tambah toko "><i class="fa fa-plus"></i></a>
			<a href="<?php echo Yii::app()->createUrl('toko/admin'); ?>" class="btn btn-danger" title="Kelola toko "><i class="fa fa-cogs"></i></a>
		</span>
	</div>
	<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
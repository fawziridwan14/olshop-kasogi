<?php
/* @var $this BankController */
/* @var $model Bank */

$this->breadcrumbs=array(
	'Banks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Bank', 'url'=>array('index')),
	array('label'=>'Create Bank', 'url'=>array('create')),
	array('label'=>'Update Bank', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Bank', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Bank', 'url'=>array('admin')),
);
?>
<br>  
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">View Bank #<?php echo $model->id; ?></h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('bank/index'); ?>" class="btn btn-info" title="List Bank Transfer"><i class="fa fa-bars"></i></a>
			<a href="<?php echo Yii::app()->createUrl('bank/create'); ?>" class="btn btn-primary" title="Tambah Bank Transfer"><i class="fa fa-plus"></i></a>
			<a href="<?php echo Yii::app()->createUrl('bank/update/'.$model->id); ?>" class="btn btn-warning" title="Update Bank Transfer"><i class="fa fa-pencil"></i></a>
			<a href="<?php echo Yii::app()->createUrl('bank/delete/'.$model->id); ?>" class="btn btn-danger" title="Delete Bank Transfer"><i class="fa fa-trash"></i></a>
			<a href="<?php echo Yii::app()->createUrl('bank/admin'); ?>" class="btn btn-success" title="Kelola Bank Transfer"><i class="fa fa-cogs"></i></a>
		</span>
	</div>
	<div style="margin: 10px;">
		<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				'id',
				'nama_bank',
				'pemilik',
				'no_rek',
				'logo',
			),
		)); ?>		
	</div>	
	<br>
</div>	
<?php
/* @var $this BankController */
/* @var $model Bank */

$this->breadcrumbs=array(
	'Banks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Bank', 'url'=>array('index')),
	array('label'=>'Create Bank', 'url'=>array('create')),
	array('label'=>'View Bank', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Bank', 'url'=>array('admin')),
);
?>
 
<br>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Ubah Bank Tranfer <?php echo $model->id; ?></h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('bank/index'); ?>" class="btn btn-primary" title="List Bank Transfer"><i class="fa fa-bars"></i></a>
			<!-- echo Yii::app()->createUrl('bank/delete/'.$model->id); -->
			<a href="<?php echo Yii::app()->createUrl('bank/'.$model->id); ?>" class="btn btn-info" title="View Bank Transfer"><i class="fa fa-search"></i></a>
			<a href="<?php echo Yii::app()->createUrl('bank/create'); ?>" class="btn btn-success" title="Tambah Bank Transfer"><i class="fa fa-plus"></i></a>
			<a href="<?php echo Yii::app()->createUrl('bank/admin'); ?>" class="btn btn-danger" title="Kelola Bank Transfer"><i class="fa fa-cogs"></i></a>
		</span>
	</div>
	<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
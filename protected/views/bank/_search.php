<?php
/* @var $this BankController */
/* @var $model Bank */
/* @var $form CActiveForm */
?>

<div class="form-group" style="margin: 10px;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
 
	<div class="form-group">
		<?php echo $form->label($model,'id', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'id', array('class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'nama_bank', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'nama_bank',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'pemilik', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'pemilik',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'no_rek', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'no_rek',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'logo', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'logo',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group pull-left">
		<?php echo CHtml::submitButton('Search', array('class'=>'btn btn-primary btn-flat')); ?>
	</div>
	<br><br>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
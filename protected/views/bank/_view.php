<?php
/* @var $this BankController */
/* @var $data Bank */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_bank')); ?>:</b>
	<?php echo CHtml::encode($data->nama_bank); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pemilik')); ?>:</b>
	<?php echo CHtml::encode($data->pemilik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_rek')); ?>:</b>
	<?php echo CHtml::encode($data->no_rek); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('logo')); ?>:</b>
	<?php echo CHtml::encode($data->logo); ?>
	<br />


</div>
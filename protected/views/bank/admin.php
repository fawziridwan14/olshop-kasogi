<?php
/* @var $this BankController */
/* @var $model Bank */

$this->breadcrumbs=array(
	'Banks'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Bank', 'url'=>array('index')),
	array('label'=>'Create Bank', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#bank-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<br><br>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Kelola Bank</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('bank/index'); ?>" class="btn btn-primary"><i class="fa fa-bars"></i> List Bank Transfer</a>
			<a href="<?php echo Yii::app()->createUrl('bank/create'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Bank Transfer</a>
		</span>
	</div>
	<div style="margin: 10px;">
		<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
		<div class="search-form" style="display:none">
		<?php $this->renderPartial('_search',array(
			'model'=>$model,
		)); ?>		
	</div>
	</div><!-- search-form -->	
	<div style="margin: 10px;">
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'bank-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'id',
			'nama_bank',
			'pemilik',
			'no_rek',
			'logo',
			array(
				'class'=>'CButtonColumn',
			),
		),
	)); ?>		
	</div>	
</div>

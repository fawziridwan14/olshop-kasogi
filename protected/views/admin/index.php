<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
// $this->layout='admin_page';
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
  'Login',
);
?> 

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login Page | AdminLTE</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/fontawesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/css/skins/skin-blue.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/css/skins/skin-black.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/css/skins/skin-green.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/datatables/dataTables.bootstrap.css">
<!-- select2 css -->
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/plugin/select2/css/select2.min.css">
<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/plugin/select2/js/select2.full.min.js"></script>

<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>

<!-- <script src="/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script> -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/dist/js/app.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
</head>
<body class="hold-transition login-page">
<div class="login-box"> 
  <div class="login-box-body">
    <div class="login-logo">
      <a href="<?php echo Yii::app()->createUrl('site/index'); ?>"><span>King </span><b>Collection</b><br>Login Page</a>
    </div>
  </div>
  <!-- /.login-logo --> 
  <div class="login-box-body">
    <p class="login-box-msg">Fields with <span class="required">*</span> are required.</p>

    <div class="form">
      <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
          'validateOnSubmit'=>true,
        ),
      )); ?>


    </div>
      <div class="form-group has-feedback">
        <?php echo $form->label($model,'username'); ?>
        <?php echo $form->textField($model,'username', array('size'=>30, 'placeholder'=>'Username','class'=>'form-control')); ?>
        <?php echo $form->error($model,'username'); ?>   
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>   
        <!-- <input type="email" class="form-control" placeholder="Email"> -->
      </div>
      <div class="form-group has-feedback">
        <?php echo $form->labelEx($model,'password'); ?>
        <?php echo $form->passwordField($model,'password',array('size'=>30, 'placeholder'=>'Password','class'=>'form-control')); ?>
        <?php echo $form->error($model,'password'); ?>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <!-- <input type="password" class="form-control" placeholder="Password"> -->
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="col-xs-8">
            <?php //echo "Belum punya akun ? klik ".CHtml::link('Disini',array('pelanggan/daftar')); ?>
          </div>        
          <!-- /.col -->
          <div class="col-xs-4">
            <?php 
               echo CHtml::submitButton('Login', 
                   array(
                       'class' => 'btn btn-primary btn-block btn-flat',
                   )); 
             ?>
          </div>
          </div>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>

<?php $this->endWidget(); ?>
</div><!-- form -->

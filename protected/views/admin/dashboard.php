<?php
/* @var $this AdminController */

/*$this->breadcrumbs=array(
  'Admin',
);*/
?>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cart-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">
            Order Confirm                  
          </span> 
          <span class="info-box-number">
          <!-- 10 -->
            <?php 
              $sql = Yii::app()->db->createCommand("select * from orders where payment_status='1'")->queryAll(); //It's return the Array
                $data = count($sql);
                if ($data == 0){
                  $data ='0';
                }else{
                 $data = count($sql); 
                }
                   echo $data;
                   echo $data["id"];
            ?>
          </span>
          <br>
            <div align="right">
              <a class="label label-primary" href="<?php echo Yii::app()->request->baseUrl; ?>/orders/admin"><i class="fa fa-search"></i> Lihat Detail</a>
            </div>
        </div>
      </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-shopping-cart"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Pending Order</span>
          <span class="info-box-number">
            <?php 
              $sql = Yii::app()->db->createCommand("select * from orders where payment_status='0'")->queryAll(); //It's return the Array
                $data = count($sql);
                if ($data == 0){
                  $data ='0';
                }else{
                 $data = count($sql); 
                }
                   echo $data;
                   echo $data["id"];
            ?>
          </span>
          <br>
            <div align="right">
              <a class="label label-danger" href="<?php echo Yii::app()->request->baseUrl; ?>/orders/admin"><i class="fa fa-search"></i> Lihat Detail</a>
            </div>
        </div>
      </div>
    </div>

    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-product-hunt"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Product Stock</span>
          <span class="info-box-number">
            <?php 
            $sql = Yii::app()->db->createCommand("select * from product where id")->queryAll(); //It's return the Array
              $data = count($sql);
              if ($data == 0){
                $data ='0';
              }else{
               $data = count($sql); 
              }
                 echo $data;
                 echo $data["id"];
            ?>                
          </span>
          <br>
            <div align="right">
              <a class="label label-success" href="<?php echo Yii::app()->request->baseUrl; ?>/product/admin"><i class="fa fa-search"></i> Lihat Detail</a>
            </div>
        </div>
      </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">List Member</span>
          <span class="info-box-number">
            <?php 
            $sql = Yii::app()->db->createCommand("select * from customer where customer_id")->queryAll(); //It's return the Array
              $data = count($sql);
              if ($data == 0){
                $data ='0';
              }else{
               $data = count($sql); 
              }
                 echo $data;
                         echo $data["customer_id"];
            ?>
          </span>
          <br>
            <div align="right">
              <a class="label label-warning" href="<?php echo Yii::app()->request->baseUrl; ?>/customer/index"><i class="fa fa-search"></i> Lihat Detail</a>
            </div>
        </div>
      </div>
    </div>
</div>
<div>
</div>
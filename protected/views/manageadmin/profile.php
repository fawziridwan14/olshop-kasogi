<?php 
$this->breadcrumbs=array(
	'User Profile',
);
?>
<br>
<?php 
$x = Admin::model()->findByPk(1);
foreach($x as $model): ?>
<?php endforeach; ?>

<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Profil User</h3>
		<span style="float: right;padding-top: 15px;">
			<left>
			<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.Yii::app()->user->adminImage.'','image', array("class"=>"profile-user-img img-responsive img-circle", "alt"=>"User profile picture")); ?>
			<h3 class="profile-username text-center"><?php echo $model->username; ?></h3>
			<p class="text-muted text-center"><?php echo $model->rule; ?></p>				
			</left>
			<!-- <a href="<?php //echo Yii::app()->createUrl('manageadmin/create'); ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah User Admin</a> -->
			<!-- <a href="<?php //echo Yii::app()->createUrl('manageadmin/admin'); ?>" class="btn btn-danger"><i class="fa fa-cogs"></i> Kelola User Admin</a> -->
		</span>
	</div>
	<div class="table table-responsive">
		<table class="table table-bordered">
			<tr class="form-group">
				<th valign="top" class="label-control">Id</th>
				<td valign="top" class="form-control"><?php echo Yii::app()->user->id; ?></td>			
			</tr>
			<tr class="form-group">
				<th valign="top" class="label-control">Username</th>
				<td valign="top" class="form-control"><?php echo Yii::app()->user->username; ?></td>			
			</tr>
			<tr class="form-group">
				<th valign="top" class="label-control">Email</th>
				<td valign="top" class="form-control"><?php echo Yii::app()->user->adminEmail; ?></td>			
			</tr>
			<tr class="form-group">
				<th valign="top" class="label-control">Hak Akses</th>
				<td valign="top" class="form-control"><?php echo Yii::app()->user->rule; ?></td>			
			</tr>
		</table>
	</div>
</div>
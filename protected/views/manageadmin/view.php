<?php
/* @var $this ManageadminController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Admins'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Admin', 'url'=>array('index')),
	array('label'=>'Create Admin', 'url'=>array('create')),
	array('label'=>'Update Admin', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Admin', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Admin', 'url'=>array('admin')),
);
?>
<br>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">View Admin #<?php echo $model->id; ?></h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('manageadmin/create'); ?>" class="btn btn-info" title="List User Admin Transfer"><i class="fa fa-bars"></i></a>
			<a href="<?php echo Yii::app()->createUrl('manageadmin/create'); ?>" class="btn btn-primary" title="Tambah User Admin Transfer"><i class="fa fa-plus"></i></a>
			<a href="<?php echo Yii::app()->createUrl('manageadmin/update/'.$model->id); ?>" class="btn btn-warning" title="Update User Admin Transfer"><i class="fa fa-pencil"></i></a>
			<a href="<?php echo Yii::app()->createUrl('manageadmin/delete/'.$model->id); ?>" class="btn btn-danger" title="Delete User Admin Transfer"><i class="fa fa-trash"></i></a>
			<a href="<?php echo Yii::app()->createUrl('manageadmin/admin'); ?>" class="btn btn-success" title="Kelola User Admin Transfer"><i class="fa fa-cogs"></i></a>
		</span>
	</div>
	<div style="margin: 10px;">
		<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				'id',
				'email',
				'username',
				'password',
				'image',
			),
		)); ?>
	</div>	
	<br>
</div>

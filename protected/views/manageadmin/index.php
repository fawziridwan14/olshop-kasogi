<?php
/* @var $this ManageadminController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Admins',
);

$this->menu=array(
	array('label'=>'Create Admin', 'url'=>array('create')),
	array('label'=>'Manage Admin', 'url'=>array('admin')),
);
?>

<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">List User Admin</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('manageadmin/create'); ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah User Admin</a>
			<a href="<?php echo Yii::app()->createUrl('manageadmin/admin'); ?>" class="btn btn-danger"><i class="fa fa-cogs"></i> Kelola User Admin</a>
		</span>
	</div>
	<div style="margin: 10px;">
		<?php $this->widget('zii.widgets.CListView', array(
			'dataProvider'=>$dataProvider,
			'itemView'=>'_view',
		)); ?>		
	</div>
</div> 
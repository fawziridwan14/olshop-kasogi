<?php
/* @var $this ManageadminController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Admins'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Admin', 'url'=>array('index')),
	array('label'=>'Manage Admin', 'url'=>array('admin')),
);
?>
<br>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Tambah User Admin</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('manageadmin/admin'); ?>" class="btn btn-danger"><i class="fa fa-cogs"></i> Kelola User Admin</a>
			<a href="<?php echo Yii::app()->createUrl('manageadmin/index'); ?>" class="btn btn-primary"><i class="fa fa-bars"></i> List User Admin</a>
		</span>
	</div>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
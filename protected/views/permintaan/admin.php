<?php
/* @var $this PermintaanController */
/* @var $model Permintaan 

$this->breadcrumbs=array(
	'Permintaans'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Permintaan', 'url'=>array('index')),
	array('label'=>'Create Permintaan', 'url'=>array('create')),
);
*/
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#permintaan-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3 class="box-title">Kelola Permintaan Produk</h3>
<br>
<div class="box box-primary">
	<div class="box-header">
		<span style="float: right;padding-top: 15px; margin: 5px;">
			<a href="<?php echo Yii::app()->createUrl('permintaan/index'); ?>" class="btn btn-danger btn-flat btn-sm" title="List Permintaaan"><i class="fa fa-bars"></i></a>
			<a href="<?php echo Yii::app()->createUrl('permintaan/create'); ?>" class="btn btn-info btn-flat btn-sm" title="Tambah Permintaaan"><i class="fa fa-plus"></i></a>
		</span>
	</div>
	<div style="margin: 10px;">
	<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
	<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
	</div><!-- search-form -->

	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'permintaan-grid',
		'dataProvider'=>$model->search(),
		'pagerCssClass'=>'pagination',
	    'ajaxUpdate'=>true, 
		'filter'=>$model,
		'columns'=>array(
			'id',
			'tanggal',
			array('name'=>'supplier_id',
				'type'=>'html',
				'value'=>'$data->supplier_relasi->nama_supplier',
				'sortable'=>TRUE,
				'filter'=>CHtml::listData(Supplier::model()->findAll(),'id','nama_supplier'),
			),
			array('name'=>'produk_id',
				'type'=>'html',
				'value'=>'$data->product_relasi->product_name',
				'sortable'=>TRUE,
				'filter'=>CHtml::listData(Product::model()->findAll(),'id','product_name'),
			),
			// 'supplier_id',
			// 'produk_id',
			'jumlah',
			array(
				'header'=>'Aksi',
				'class'=>'CButtonColumn',
				'template'=>'{update}{view}{delete}'

			),
		),
	)); ?>
	</div>
</div>

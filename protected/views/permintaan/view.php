<?php
/* @var $this PermintaanController */
/* @var $model Permintaan 

$this->breadcrumbs=array(
	'Permintaans'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Permintaan', 'url'=>array('index')),
	array('label'=>'Create Permintaan', 'url'=>array('create')),
	array('label'=>'Update Permintaan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Permintaan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Permintaan', 'url'=>array('admin')),
); */
?>

<h1>Detail Permintaan Produk #<?php echo $model->id; ?></h1>

<br>
<div class="box box-primary">
	<div class="box-header">
		<span style="float: right;padding-top: 15px; margin: 5px;">
			<a href="<?php echo Yii::app()->createUrl('permintaan/index'); ?>" class="btn btn-info btn-flat btn-sm" title="List Permintaaan"><i class="fa fa-bars"></i></a>
			<a href="<?php echo Yii::app()->createUrl('permintaan/create'); ?>" class="btn btn-success btn-flat btn-sm" title="Tambah Permintaaan Produk"><i class="fa fa-plus"></i></a>
			<a href="<?php echo Yii::app()->createUrl('permintaan/update', array('id'=>$model->id)); ?>" class="btn btn-warning btn-flat btn-sm" title="Update Permintaaan Produk"><i class="fa fa-edit"></i></a>
			<a href="<?php echo Yii::app()->createUrl('permintaan/admin'); ?>" class="btn btn-danger btn-flat btn-sm" title="Kelola Permintaaan"><i class="fa fa-cogs"></i></a>
		</span>
	</div>
	<div style="margin: 10px;">
		<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				'id',
				'tanggal',
				array(
					'value' => $model->supplier_relasi->nama_supplier, 
					'label' => 'Nama Supplier'
				),
				array(
					'value' => $model->product_relasi->product_name, 
					'label' => 'Nama Produk'
				),
				'jumlah',
			),
		)); ?>
	</div>
	<br>
</div>
<?php
/* @var $this PermintaanController */
/* @var $model Permintaan 

$this->breadcrumbs=array(
	'Permintaans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Permintaan', 'url'=>array('index')),
	array('label'=>'Create Permintaan', 'url'=>array('create')),
	array('label'=>'View Permintaan', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Permintaan', 'url'=>array('admin')),
);*/
?>

<h3 class="box-title">Update Permintaan #<?php echo $model->id; ?></h3>
<br>
<div class="box box-primary">
	<div class="box-header">
		<span style="float: right;padding-top: 15px; margin: 5px;">
			<a href="<?php echo Yii::app()->createUrl('permintaan/index'); ?>" class="btn btn-info btn-sm btn-flat" title="List Permintaan"><i class="fa fa-cogs"></i></a>
			<a href="<?php echo Yii::app()->createUrl('permintaan/create'); ?>" class="btn btn-primary btn-sm btn-flat" title="Create Permintaan"><i class="fa fa-plus"></i></a>
			<a href="<?php echo Yii::app()->createUrl('permintaan/view', array('id'=>$model->id)); ?>" class="btn btn-success btn-sm btn-flat" title="Lihat Permintaan"><i class="fa fa-search"></i></a>
			<a href="<?php echo Yii::app()->createUrl('permintaan/admin'); ?>" class="btn btn-danger btn-sm btn-flat" title="Kelola Permintaan"><i class="fa fa-edit"></i></a>
		</span>
	</div>
	<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
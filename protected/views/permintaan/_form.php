<?php
/* @var $this PermintaanController */
/* @var $model Permintaan */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'permintaan-form',
	'enableAjaxValidation'=>false,
)); ?>

	<!-- <p class="note">Fields with <span class="required">*</span> are required.</p> -->

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered">
				<thead style="width: 100%;">
					<th><?php echo $form->labelEx($model,'tanggal', array('class'=>'label-control')); ?></th>
					<th><?php echo $form->labelEx($model,'supplier_id', array('class'=>'label-control')); ?></th>
				</thead>
				<tbody>
					<tr>
						<td>
							<?php echo $form->textField($model,'tanggal', array('class'=>'form-control', 'value'=>date('Y-m-d'), 'readOnly'=>true)); ?>
							<?php echo $form->error($model,'tanggal'); ?>
						</td>
						<td>
							<?php echo $form->dropDownList($model,'supplier_id',CHtml::listData(Supplier::model()->findAll(), 'id','nama_supplier'), array(
								'empty'=>'Pilih Supplier',
								'class'=>'form-control',
							));
							?>
							<?php echo $form->error($model,'supplier_id'); ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>	

	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered">
				<thead style="width: 100%;">
					<th><?php echo $form->labelEx($model,'produk_id', array('class'=>'label-control')); ?></th>
					<th><?php echo $form->labelEx($model,'jumlah', array('class'=>'label-control')); ?></th>
				</thead>
				<tbody>
					<tr>
						<td>
							<?php echo $form->dropDownList($model,'produk_id',CHtml::listData(Product::model()->findAll(), 'id','product_name'), array('empty'=>'Pilih Produk','class'=>'form-control',));?>
							<?php echo $form->error($model,'produk_id'); ?>
						</td>
						<td>
							<?php echo $form->textField($model,'jumlah', array('class'=>'form-control','placeholder'=>'Jumlah')); ?>
							<?php echo $form->error($model,'jumlah'); ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="form-group pull-right">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-primary btn-flat')); ?>				
			</div>
		</div>		
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
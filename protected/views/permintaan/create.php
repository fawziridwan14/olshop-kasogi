<?php
/* @var $this PermintaanController */
/* @var $model Permintaan */
/*
$this->breadcrumbs=array(
	'Permintaans'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Permintaan', 'url'=>array('index')),
	array('label'=>'Manage Permintaan', 'url'=>array('admin')),
);*/
?>

<h3 class="box-title">Permintaan Produk</h3>
<br>
<div class="box box-primary">
	<div class="box-header">
		<span style="float: right;padding-top: 15px; margin: 5px;">
			<a href="<?php echo Yii::app()->createUrl('permintaan/index'); ?>" class="btn btn-info btn-flat btn-sm" title="List Permintaaan"><i class="fa fa-search"></i></a>
			<a href="<?php echo Yii::app()->createUrl('permintaan/admin'); ?>" class="btn btn-danger btn-flat btn-sm" title="Kelola Permintaaan"><i class="fa fa-cogs"></i></a>
		</span>
	</div>
	<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>